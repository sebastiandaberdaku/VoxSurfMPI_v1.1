#=================================================================
#
#	Author: Sebastian Daberdaku
#	Project: VoxSurfMPI	v1.1
#
#	This makefile searches SRC_DIR and its subdirectories
#	recursively for source files and builds them.
#
#	Object and dependency files can be placed in separate 
#	directories.
#
#=================================================================
CXX := /home/daberdak/compiler/gcc-5.2.0/bin/g++
MPCC := mpCC -q64 -compiler $(CXX)
MKDIR_P := @mkdir -p 

mpiP_root := /home/daberdak/mpiP
binutils_root := /home/daberdak/binutils-2.26
libunwind_root := /home/daberdak/libunwind
mpiP_LIBS := -L$(mpiP_root)/lib -L$(binutils_root)/libiberty -L$(binutils_root)/bfd -L$(libunwind_root)/lib64 -R$(libunwind_root)/lib64 -lmpiP -lm -lbfd -liberty -lunwind
BOOST_LIBS := -lboost_program_options -lboost_regex
GCC_LIBS := -I/home/daberdak/compiler/gcc-5.2.0/include -L/home/daberdak/compiler/gcc-5.2.0/lib64 -static-libstdc++

GPPOPT := -c -std=c++11 -MMD -O5 -mcpu=power7 -mtune=power7 -funroll-loops
LDOPT := -std=c++11 -O5 -mcpu=power7 -mtune=power7
LIBS := $(GCC_LIBS) $(BOOST_LIBS) #$(mpiP_LIBS) 

BIN_DIR := bin
OBJ_DIR := bin/obj
DEP_DIR := bin/dep
OUT_DIR := bin/output
SRC_DIR := src

# Defines: just add the defines to this variable
DEFS := -D NDEBUG #-D NO_OUTPUT_TEST #-D RANGECHECK_TEST #-D TIGHT_PACKING #-D NO_OUTPUT_TEST

# Make does not offer a recursive wildcard function, so here's one:
rwildcard = $(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

# Recursively find all source files in SRC_DIR
SOURCES := $(call rwildcard,$(SRC_DIR)/,*.cpp)
OBJECTS := $(SOURCES:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)
DEPENDENCIES := $(SOURCES:$(SRC_DIR)/%.cpp=$(DEP_DIR)/%.d)
EXECUTABLE := $(BIN_DIR)/VoxSurfMPI_v1.1

.PHONY: all clean

all: $(EXECUTABLE) 
	
$(EXECUTABLE): $(OBJECTS)
	$(MPCC) $(LDOPT) $^ -o $@ $(LIBS)
	$(MKDIR_P) $(OUT_DIR)
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(MKDIR_P) $(@D) $(@D:$(OBJ_DIR)%=$(DEP_DIR)%)
	$(MPCC) $(GPPOPT) $< -o $@ -MF $(@:$(OBJ_DIR)%.o=$(DEP_DIR)%.d) $(DEFS)
	
clean: 
	rm -rf $(EXECUTABLE) $(OBJ_DIR) $(DEP_DIR)

-include $(DEPENDENCIES)
	
