/*
 * Pockets.cpp
 *
 *  Created on: Feb 4, 2014
 *      Author: sebastian
 */
#include "Pocket.h"
#include <queue>
#include <iostream>
#include <fstream>

/**
 * Constructor of the class.
 */
Pocket::Pocket(int minMargin, int maxMargin) : minMargin(minMargin), maxMargin(maxMargin),
	isPocket(false), matchesLeft (false), matchesRight(false) { }
/**
 * Destructor of the class.
 */
Pocket::~Pocket() { }
/**
 * This method calculates the left and right borders of the current
 * potential pocket. If a voxel has its x coordinate equal to the
 * minimum margin, then it belongs to the left border. Similarly,
 * if its x coordinate equals the maximum margin, then it belongs to
 * the right border.
 */
void Pocket::calculateBorders() {
	std::list<voxel>::iterator it;
	for (it = pocketVoxels.begin(); it != pocketVoxels.end(); ++it) {
		if (it->ix == minMargin)
			leftBorder.push_back(*it);
		else if (it->ix == maxMargin)
			rightBorder.push_back(*it);
	}
}

/**
 * This method matches the candidate pockets on adjacent slices, in order
 * to determine if the candidate pockets are solvent-accessible or isolated.
 * If a candidate pocket does not match, than it is solvent accessible.
 *
 * \param leftList	List of pockets belonging to the left slice.
 * \param rightList List of pockets belonging to the right slice.
 */
void Pocket::matchPocketSlices(std::list<Pocket> &leftList, std::list<Pocket> &rightList) {
	std::list<Pocket>::iterator lIt, rIt;
	std::list<voxel>::iterator leftVox, rightVox;
	for (lIt = leftList.begin(); lIt != leftList.end(); ++lIt) {
		for (rIt = rightList.begin(); rIt != rightList.end(); ++rIt) {
			bool match = false;
			leftVox = lIt->rightBorder.begin();
			while (leftVox != lIt->rightBorder.end() && !match) {
				rightVox = rIt->leftBorder.begin();
				while (rightVox != rIt->leftBorder.end() && !match) {
					if (neighbors(*leftVox, *rightVox)) {
						if (lIt->isPocket) {
							rIt->isPocket = true;
						} else if (rIt->isPocket) {
							lIt->isPocket = true;
						}
						lIt->matchesRight = true;
						rIt->matchesLeft = true;
						match = true;
					}
					++rightVox;
				}
				++leftVox;
			}
		}
	}
	for (rIt = rightList.begin(); rIt != rightList.end(); ++rIt) {
		if (!rIt->matchesLeft && !rIt->leftBorder.empty()) {
			rIt->isPocket = true;
		}
	}
	for (lIt = leftList.begin(); lIt != leftList.end(); ++lIt) {
		if (!lIt->matchesRight && !lIt->rightBorder.empty()) {
			lIt->isPocket = true;
		}
	}
}
/**
 * Simple method that determines if the two input voxels are neighbors.
 * The given voxels belong to two adjacent grid slices.
 *
 * \param left	The first voxel.
 * \param right The second voxel.
 * \return 		True if left and right are neighbors, false otherwise.
 */
bool Pocket::neighbors(voxel const& left, voxel const& right) {
	int dy = left.iy - right.iy;
	int dz = left.iz - right.iz;
 
	if (-1 <= dy && dy <= 1 && -1 <= dz && dz <= 1)
		return true;
	else
		return false;
}
/** Prints the 3D voxelized representation to file using the PCD
 * (Point Cloud Data) file format.
 *
 * Each PCD file contains a header that identifies and declares
 * certain properties of the point cloud data stored in the file.
 * The header of a PCD must be encoded in ASCII.
 * Storing point cloud data in both a simple ascii form with each
 * point on a line, space or tab separated, without any other
 * characters on it, as well as in a binary dump format, allows
 * us to have the best of both worlds: simplicity and speed,
 * depending on the underlying application. The ascii format
 * allows users to open up point cloud files and plot them using
 * standard software tools like gnuplot or manipulate them using
 * tools like sed, awk, etc.
 *
 * For a detailed description of the PCD (Point Cloud Data) file
 * format specification see:
 * http://pointclouds.org/documentation/tutorials/pcd_file_format.php
 *
 * \param filename		Name of the output file. The '.pcd' extension
 * 						is added automatically.
 * \param pockets		List of the pockets in the current slice.
 * \param resolution 	resolution^3 = #voxels/Å^3
 * \param ptran			Translation vector to the original coordinate system.
 *
 * \throws ofstream::failure
 */
void Pocket::outputPocketPCDModel(std::string const & filename, std::list<Pocket> const & pockets,
		float resolution, point3D ptran) {
	std::queue<voxel> pocketVoxels;
	std::list<Pocket>::const_iterator it;
	std::list<voxel>::const_iterator vx;
	for (it = pockets.begin(); it != pockets.end(); ++it) {
#ifndef POCKETS_TEST
		if (it->isPocket) {
			for (vx = it->pocketVoxels.begin(); vx != it->pocketVoxels.end(); ++vx) {
				pocketVoxels.push(*vx);
			}
		}
#else
		for (vx = it->pocketVoxels.begin(); vx != it->pocketVoxels.end(); ++vx) {
			pocketVoxels.push(*vx);
		}
#endif
	}
	if (pocketVoxels.empty())
		return;
	std::ofstream file_stream;
	file_stream.open((filename + ".pcd").c_str());
	if (!file_stream.is_open()) {
		throw std::ofstream::failure("Error opening output file.");
	}
	std::cout << "Output PCD model for pockets in slice " << filename.substr(filename.find_last_of("-") + 1) <<"\n";
	/* File header */
	file_stream << "# .PCD v.7 - Point Cloud Data file format\n"
			<< "VERSION .7\n"
			<< "FIELDS x y z\n"
			<< "SIZE 4 4 4\n"
			<< "TYPE F F F\n"
			<< "COUNT 1 1 1\n"
			<< "WIDTH " << pocketVoxels.size() << "\n"
			<< "HEIGHT 1\n"
			<< "VIEWPOINT 0 0 0 1 0 0 0\n"
			<< "POINTS " << pocketVoxels.size() << "\n"
			<< "DATA ascii";

	while (!pocketVoxels.empty()) {
		file_stream << "\n" << (pocketVoxels.front().ix - ptran.x)/resolution << " "
				<< (pocketVoxels.front().iy - ptran.y)/resolution << " "
				<< (pocketVoxels.front().iz - ptran.z)/resolution;
		pocketVoxels.pop();
	}
	file_stream.close();
}; /* outputPocketPCDModel() */
