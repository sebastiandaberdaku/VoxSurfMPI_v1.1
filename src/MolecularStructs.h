/*
 * MolecularStructs.h
 *
 *  Created on: Dec 24, 2013
 *      Author: daberdaku
 */

#ifndef MOLECULARSTRUCTS_H_
#define MOLECULARSTRUCTS_H_

#include "param.h"

//#pragma pack(push)  /* push current alignment to stack */
//#pragma pack(1)     /* set alignment to 1 byte boundary */
typedef struct atom {
	short snum; /**< Atom serial number. */
	std::string name; /**< Atom name. */
	char alt; /**< Alternate location indicator. */
	std::string resname; /**< Residue name. */
	char chain; /**< Chain identifier. */
	short resnum; /**< Residue sequence number. */
	char insertion_residue_code; /**< Code for insertion of residues. */
	float x /**< Orthogonal coordinates for X in Angstroms. */,
			y /**< Orthogonal coordinates for Y in Angstroms. */,
			z /**< Orthogonal coordinates for Z in Angstroms. */;
	float occupancy /**< Occupancy. */,
			tempFactor /**< Temperature factor (Default = 0.0). */;
	std::string segID /**< Segment identifier, left-justified. */,
			element /**< Element symbol, right-justified. */,
			charge /**< Charge on the atom. */;
} atom;
//#pragma pack(pop)   /* restore original alignment from stack */

typedef struct header {
	std::string classification; /**< Classifies the molecule(s). */
	std::string depDate; /**< Deposition date. This is the date the
                              coordinates  were received at the PDB. */
	std::string idCode; /**< This identifier is unique within the PDB. */
} header;

#endif /* MOLECULARSTRUCTS_H_ */
