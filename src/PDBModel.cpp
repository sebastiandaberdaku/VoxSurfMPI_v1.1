/**
 * Implementation of the PDBModel class.
 */
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>

#include "ParsingPDBException.h"
#include "MolecularStructs.h"
#include "PDBModel.h"

using namespace std;
/**
 * Constructor
 */
PDBModel::PDBModel(int rank) : pdbline(0), rank(rank) { }
/**
 * Destructor
 */
PDBModel::~PDBModel() { }

/** Parses one line from a PDB file. This method gets a string containing the line
 * to be parsed as an input, and parses it.
 *
 * \param line	The input line.
 */
void PDBModel::parseLine(const char* line) {

	lineType lt = getLineType(line);

	if (lt == ATOM) {
		int snum = -1; /**< Atom serial number. */
		char name[5] = { '\0' }; /**< Atom name. */
		char alt = '\0'; /**< Alternate location indicator. */
		char resname[4] = { '\0' }; /**< Residue name. */
		char chain = '\0'; /**< Chain identifier. */
		int resnum = -1; /**< Residue sequence number. */
		char insertion_residue_code = '\0'; /**< Code for insertion of residues. */
		float x /**< Orthogonal coordinates for X in Angstroms. */,
				y /**< Orthogonal coordinates for Y in Angstroms. */,
				z /**< Orthogonal coordinates for Z in Angstroms. */;
		float occupancy /**< Occupancy. */,
				tempFactor/**< Temperature factor (Default = 0.0). */;
		char segID[5] = { '\0' } /**< Segment identifier, left-justified. */,
				element[3] = { '\0' } /**< Element symbol, right-justified. */,
				charge[3] = { '\0' } /**< Charge on the atom. */;
		int numscan = sscanf(line, ATOM_LINE_FORMAT,
				/* ATOM  %5d%*1c%4c%1c%3c%*1c%1c%4d%1c%*3c%8f%8f%8f%6f%6f%*6c%4c%2c%2c */
				&snum, name, &alt, resname, &chain, &resnum,
				&insertion_residue_code, &x, &y, &z, &occupancy,
				&tempFactor, segID, element, charge);
		/* numscan contains the index of the last successfully parsed parameter,
		 * total is 15. If numscan is less than 15, it means that some fields have
		 * not been parsed correctly. The last field (charge) is optional, so we do
		 * check the first 14 only.  */
		if (numscan < 14) {
			stringstream error;
			error << "Failed parsing \"ATOM\" line, number: " << pdbline
					<< " in PDB file: " << filename << ".";
			stringstream cause;
					cause << "Number of parsed fields is: " << numscan
					<< ". Should be 14 or 15 (because charge is optional).";
			throw ParsingPDBException(error.str(), "PDBModel::parseLine",
					"Incorrect PDB file format. " + cause.str());
		}
		atom currentAtom;

		currentAtom.snum = snum;
		currentAtom.name = string(name);
		trim(currentAtom.name);
		currentAtom.alt = alt;
		currentAtom.resname = string(resname);
		trim(currentAtom.resname);
		currentAtom.chain = chain;
		currentAtom.resnum = resnum;
		currentAtom.insertion_residue_code = insertion_residue_code;
		currentAtom.x=x;
		currentAtom.y=y;
		currentAtom.z=z;
		currentAtom.occupancy=occupancy;
		currentAtom.tempFactor=tempFactor;
		currentAtom.segID = string(segID);
		trim(currentAtom.segID);
		currentAtom.element = string(element);
		trim(currentAtom.element);
		currentAtom.charge = string(charge);
		trim(currentAtom.charge);
		atomsInModel.push_back(currentAtom);
	}
	else if (lt == HEADER) {

		char classification[41] = { '\0' }; /**< Classifies the molecule(s). */
		char depDate[10] = { '\0' }; /**< Deposition date. This is the date the
                                          coordinates  were received at the PDB. */
		char idCode[5] = { '\0' }; /**< This identifier is unique within the PDB. */
		int numscan = sscanf(line, HEADER_LINE_FORMAT,
				/* HEADER%*4c%40c%9c%*3c%4c */
				classification, depDate, idCode);
		/* numscan contains the index of the last successfully parsed parameter,
		 * total is 3. If numscan is less than 3, it means that some fields have
		 * not been parsed correctly. */
		if (numscan != 3) {
			stringstream error;
			error << "Failed parsing \"HEADER\" line, number: " << pdbline
					<< " from PDB file: " << filename << ".";
			stringstream cause;
			cause << "Number of parsed fields is: " << numscan
					<< ". Should be 3.";
			throw ParsingPDBException(error.str(), "PDBModel::parseLine",
					"Incorrect PDB file format. " + cause.str());
		}
		PDBHeader.classification = string(classification);
		trim(PDBHeader.classification);
		PDBHeader.depDate = string(depDate);
		PDBHeader.idCode = string(idCode);
		if (!rank) {
			cout << "PDB name: " << PDBHeader.idCode << "\n";
			cout << "classification: " << PDBHeader.classification << "\n";
			cout << "deposition date: " << PDBHeader.depDate << "\n";
		}
	}
	else
		return;
} /* parseLine() */

/** Loads the PDB file. This method parses the input PDB file one line at a time.
 * Each line is parsed by the parseLine() method.
 *
 * \param pdbfile	Input PDB filename.
 */
void PDBModel::loadPDB(char const * pdbfile) {
	std::ifstream file_stream;
	file_stream.open(pdbfile);
	if (!file_stream.is_open()) {
		throw ParsingPDBException("Failed opening input PDB file: " + string(pdbfile),
				"PDBModel::loadPDB",
				"Closed file stream.");
	}
	filename = string(pdbfile);
	//read
	char line[255];
	while (file_stream.getline(line, 255)) {
		++pdbline;
		parseLine(line);
	}
	file_stream.close();
} /* loadPDB */

/** Decides the record type contained in the current line.
 * Compliant with the PDB File Format Version 3.30 (Nov. 21, 2012)
 * \param line	The line to be analyzed.
 * \return	The type of line.
 */
lineType PDBModel::getLineType(char const * line) {
	if (line[0] == '\0')
		return OTHER;

	string word(line, 0, 6);
	if (word == "DBREF ")
		return DBREF;
	else if (word == "SEQRES")
		return SEQRES;
	else if (word == "ATOM  ")
		return ATOM;
	else if (word == "HETATM")
		return HETATM;
	else if (word == "MASTER")
		return MASTER;
	else if (word == "ENDMDL")
		return ENDMDL;
	else if (word == "END   " || word == "END  " || word == "END "
			|| word == "END")
		return END;
	else if (word == "HEADER" )
		return HEADER;
	else if (word == "OBSLTE" || word == "TITLE " || word == "SPLIT "
			|| word == "CAVEAT" || word == "COMPND" || word == "SOURCE"
			|| word == "KEYWDS" || word == "EXPDTA" || word == "NUMMDL"
			|| word == "MDLTYP" || word == "AUTHOR"	|| word == "REVDAT"
			|| word == "SPRSDE" || word == "JRNL  " || word == "REMARK"
			|| word == "DBREF1" || word == "DBREF2"	|| word == "ANISOU"
			|| word == "HELIX " || word == "SHEET " || word == "SITE  "
			|| word == "CRYST1" || word == "ORIGX1" || word == "ORIGX2"
			|| word == "ORIGX3" || word == "SCALE1" || word == "SCALE2"
			|| word == "SCALE3" || word == "MTRIX1" || word == "MTRIX2"
			|| word == "MTRIX3"	|| word == "SEQADV" || word == "TURN  "
			|| word == "FORMUL" || word == "HETNAM" || word == "SSBOND"
			|| word == "MODRES" || word == "CONECT" || word == "HET   "
			|| word == "CISPEP" || word == "LINK  " || word == "HETSYN")
		return OTHER;
	else if (word == "TER   " || word == "TER")
		return TER;
	else if (word == "MODEL ")
		return MODEL;
	else {
		stringstream cause;
		cause << "Unknown line type at line: " << pdbline << "in PDB file: "
				<< filename << ".";
		throw ParsingPDBException("\"" + word + "\" is not a known line type.",
				"PDBModel::getLineType", "Incorrect PDB file format. " + cause.str());
	}
} /* getLineType() */
