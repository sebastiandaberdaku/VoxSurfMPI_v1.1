/*
 * Pockets.h
 *
 *  Created on: Feb 4, 2014
 *      Author: sebastian
 */

#ifndef POCKET_H_
#define POCKET_H_

#include <list>
#include <string>
#include "GeometryStructs.h"

/**
 * Struct defining a potential pocket.
 */
class Pocket {
public:
	Pocket(int minMargin, int maxMargin);
	virtual ~Pocket();
	int minMargin; /**> The minimum margin of the current molecular slice. */
	int maxMargin; /**> The maximum margin of the current molecular slice. */
	bool isPocket;
	bool matchesLeft; /**> True if the current potential pocket has a
					   * corresponding potential pocket in the slice
					   * on the left of the current slice.*/
	bool matchesRight; /**> True if the current potential pocket has a
	   	   	   	   	   	* corresponding potential pocket in the slice
	   	   	   	   	   	* on the right of the current slice.*/
	std::list<voxel> pocketVoxels; /**> List containing all the voxels
									* composing the surface of the pocket. */
	std::list<voxel> leftBorder; /**> Voxels belonging to the current voxels
	 	 	 	 	 	 	 	  * on the left extremity of the current slice.
	 	 	 	 	 	 	 	  * Empty if no such voxels. */
	std::list<voxel> rightBorder;/**> Voxels belonging to the current voxels
 	 	  	  	  	  	  	  	  * on the right extremity of the current slice.
 	 	  	  	  	  	  	  	  * Empty if no such voxels. */
	void calculateBorders();
	static void matchPocketSlices(std::list<Pocket> &leftList, std::list<Pocket> &rightList);
	static void outputPocketPCDModel(std::string const & filename,
			std::list<Pocket> const & pockets, float resolution, point3D ptran);
	static bool neighbors(voxel const& left, voxel const& right);

};

#endif /* POCKETS_H_ */
