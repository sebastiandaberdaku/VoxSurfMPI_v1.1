/**
 * In this header we define the PDBModel class and relative methods.
 */

#ifndef PDBMODEL_H_
#define PDBMODEL_H_

#include <list>
#include "param.h"
#include "GeometryStructs.h"
#include "MolecularStructs.h"

typedef enum lineType {
	HEADER, DBREF, SEQRES, ATOM, HETATM, MASTER, ENDMDL, OTHER, TER, MODEL, END
} lineType;

/** From http://www.wwpdb.org/documentation/format33/sect9.html#ATOM
 *
ATOM

The ATOM records present the atomic coordinates for standard amino acids and
nucleotides. They also present the occupancy and temperature factor for each atom.
Non-polymer chemical coordinates use the HETATM record type. The element symbol is
always present on each ATOM record; charge is optional.

Record Format

COLUMNS        DATA TYPE       CONTENTS
--------------------------------------------------------------------------------
 1 -  6        Record name     "ATOM  "
 7 - 11        Integer         Atom serial number.
12			   Empty		   Unused
13 - 16        Atom            Atom name.
17             Character       Alternate location indicator.
18 - 20        Residue name    Residue name.
21			   Empty		   Unused
22             Character       Chain identifier.
23 - 26        Integer         Residue sequence number.
27             AChar           Code for insertion of residues.
28 - 30		   Empty		   Unused
31 - 38        Real(8.3)       Orthogonal coordinates for X in Angstroms.
39 - 46        Real(8.3)       Orthogonal coordinates for Y in Angstroms.
47 - 54        Real(8.3)       Orthogonal coordinates for Z in Angstroms.
55 - 60        Real(6.2)       Occupancy.
61 - 66        Real(6.2)       Temperature factor (Default = 0.0).
67 - 72		   Empty		   Unused
73 - 76        LString(4)      Segment identifier, left-justified.
77 - 78        LString(2)      Element symbol, right-justified.
79 - 80        LString(2)      Charge on the atom.

Example:
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
ATOM    145  N   VAL A  25      32.433  16.336  57.540  1.00 11.92      A1   N
ATOM    146  CA  VAL A  25      31.132  16.439  58.160  1.00 11.85      A1   C
ATOM    147  C   VAL A  25      30.447  15.105  58.363  1.00 12.34      A1   C
ATOM    148  O   VAL A  25      29.520  15.059  59.174  1.00 15.65      A1   O
ATOM    149  CB AVAL A  25      30.385  17.437  57.230  0.28 13.88      A1   C
ATOM    150  CB BVAL A  25      30.166  17.399  57.373  0.72 15.41      A1   C
ATOM    151  CG1AVAL A  25      28.870  17.401  57.336  0.28 12.64      A1   C
ATOM    152  CG1BVAL A  25      30.805  18.788  57.449  0.72 15.11      A1   C
ATOM    153  CG2AVAL A  25      30.835  18.826  57.661  0.28 13.58      A1   C
ATOM    154  CG2BVAL A  25      29.909  16.996  55.922  0.72 13.25      A1   C
ATOM   1660  CE2 TYR B 205      43.549  -4.115  45.779  1.00 19.60           C
ATOM    547 HD23 LEU A  34      52.930  48.946   9.332  1.00  5.92           H
 */
#define ATOM_LINE_FORMAT "ATOM  %5d%*1c%4c%1c%3c%*1c%1c%4d%1c%*3c%8f%8f%8f%6f%6f%*6c%4c%2c%2c"


/** From http://www.wwpdb.org/documentation/format33/sect2.html#HEADER
 *
HEADER

The HEADER record uniquely identifies a PDB entry through the idCode field. This record
also provides a classification for the entry. Finally, it contains the date when the
coordinates were deposited to the PDB archive.

Record Format

COLUMNS       DATA  TYPE     FIELD             DEFINITION
------------------------------------------------------------------------------------
 1 -  6       Record name    "HEADER"
 7 - 10		  Empty			 Unused
11 - 50       String(40)     classification    Classifies the molecule(s).
51 - 59       Date           depDate           Deposition date. This is the date the
                                               coordinates  were received at the PDB.
60 - 62		  Empty			 Unused
63 - 66       IDcode         idCode            This identifier is unique within the PDB.

Example:
         1         2         3         4         5         6         7         8
12345678901234567890123456789012345678901234567890123456789012345678901234567890
HEADER    PHOTOSYNTHESIS                          28-MAR-07   2UXK
HEADER    TRANSFERASE/TRANSFERASE INHIBITOR       17-SEP-04   1XH6
HEADER    MEMBRANE PROTEIN, TRANSPORT PROTEIN     20-JUL-06   2HRT
 *
 */

#define HEADER_LINE_FORMAT "HEADER%*4c%40c%9c%*3c%4c"

class PDBModel {
public:
	PDBModel(int rank);
	int rank;
	std::string filename;
	int pdbline;
	header PDBHeader;
	std::list<atom> atomsInModel;
	void parseLine(char const * line);
	void loadPDB(char const * pdbfile);
	lineType getLineType (const char* line);
	virtual ~PDBModel();
	static bool compareAtoms(atom const & first, atom const & second) {
		return (first.x < second.x);
	};
};

#endif /* PDBMODEL_H_ */
