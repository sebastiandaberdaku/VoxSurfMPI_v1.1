#include <time.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

#include <numeric>
#include <string>
#include <utility>

#include "MolecularSurface.h"
#include "PDBModel.h"
#include "ParsingPDBException.h"
#include "Pocket.h"
#include <mpi.h>

using namespace std;

int main(int argc, char* argv[]) {
	int surfType; // Surface type
	int algorithmType = 3; // Algorithm type
	float probeRadius; // probe sphere radius
	float resolution; // resolution^3 = #voxels/Å^3
	string inname; // input filename
	string outname; // output filename
	uint16_t length, width, height; // bounding box dimensions
	point3D translation; // translation vector
	list<Pocket> candidatePockets;
	uint16_t numberOfSlices;
	uint16_t margin;
	uint16_t sliceLength;
	uint16_t numberOfAtoms;
	uint16_t atomsPerSlice;
	bool onlyHeavy = false; /**< True: heavy atoms only;
	 	 	 	 	 * false: all atoms (i.e. also Hydrogen).
	 	 	 	 	 * X-ray crystallography cannot resolve hydrogen atoms in
	 	 	 	 	 * most protein crystals, so in most PDB files, hydrogen
	 	 	 	 	 * atoms are absent. Sometimes hydrogens are added by
	 	 	 	 	 * modeling. Hydrogens are always present in PDB files
	 	 	 	 	 * resulting from NMR analysis, and usually present in
	 	 	 	 	 * theoretical models. */
	bool addProbeRadius; /**< whether to add or not the probe radius to the
	 	 	 	 	 	  * atomic radius */

	MPI_Status status; /**< MPI_Status structures are used by the message receiving functions to return data about a message.
						 *  The structure contains the following fields:
						 *  - MPI_SOURCE - id of processor sending the message
						 *  - MPI_TAG - the message tag
						 *  - MPI_ERROR - error status */
	int rank; /**< rank of the current process */
	int procs; /**< total number of processes */
	bool help = false;
	bool version = false;
	bool surf_description = false;
	bool binvox = false;
	bool license = false;
	bool algorithm_type_set = false;
	MolecularSurface * molSurface;

	/* Initialize the MPI execution environment */
	MPI_Init(&argc, &argv);
	/* Determines the size of the group associated with a communicator */
	MPI_Comm_size(MPI_COMM_WORLD, &procs);
	/* Determines the rank of the calling process in the communicator */
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	try {

		numberOfSlices = procs;

		inname=argv[1];

	    int lastindex = inname.find_last_of(".");
	    outname = inname.substr(0, lastindex);
	    surfType = stoi(argv[2]);
	    probeRadius=stof(argv[3]);
	    resolution=stof(argv[4]);

		if (help) {
			MPI_Finalize();
			return EXIT_SUCCESS;
		}
		if (version) {
			if (!rank)
				cout << "Program: " << argv[0] << ", version: "
						<< PROGRAM_VERSION << "\n";
			MPI_Finalize();
			return EXIT_SUCCESS;
		}
		if (surf_description) {
			if (!rank)
				cout << PROGRAM_NAME <<
				"The program calculates the following surfaces:\n" << VWS << SAS
						<< MS;
			MPI_Finalize();
			return EXIT_SUCCESS;
		}
		if (license) {
			if (!rank)
				DISCLAIMER
			MPI_Finalize();
			return EXIT_SUCCESS;
		}
		if (binvox && numberOfSlices > 1) {
			if (!rank)
				cout << "The program does not support printing to the binvox"
						<< " format if multiple slices are set. Aborting.\n";
			MPI_Finalize();
			return EXIT_SUCCESS;
		}
		if (algorithm_type_set && surfType != 3) {
			if (!rank)
				cout << "The -a (--algorithm_type) flag can only be specified for the MS surface.\n";
			MPI_Finalize();
			return EXIT_SUCCESS;
		}
		if (!rank) {
			PROGRAM_INFO

			/* summary of the parsed parameters */
			cout << "The specification is: \n" << "input filename: " << inname
					<< "\n";
			if (surfType == 1)
				cout << "surface: vdW, \n";
			else if (surfType == 2)
				cout << "surface: SAS, \n";
			else {
				cout << "surface: MS, \n";
				if (algorithmType == 1)
					cout << "algorithm: Rolling Sphere, \n";
				else if (algorithmType == 2) {
					cout << "algorithm: Region Growing EDT with space-optimized \n";
					cout << "data structures, \n";
				} else if (algorithmType == 3) {
					cout << "algorithm: Region Growing EDT with speed-optimized \n";
					cout << "data structures, \n";
				}
			}
			if (onlyHeavy)
				cout << "Ignoring Hydrogen atoms in the surface calculation\n";

			cout << "probe radius: " << probeRadius << ", \n";
			cout << "resolution: " << resolution << ", \n";
			cout << "number of slices: " << numberOfSlices << ", \n";
			cout << "output filename: " << outname << "\n\n";
			cout << "Loading PDB file...\n";
		}
		PDBModel model(rank);
		model.loadPDB(inname.c_str());
		if (!rank)
			cout << "Initializing parameters\n";

		if (surfType == 1) {		//vdW
			addProbeRadius = false;
		} else { 					//SAS and MS
			addProbeRadius = true;
		}

		MolecularSurface::boundingBox(model.atomsInModel, probeRadius, resolution,
				onlyHeavy, addProbeRadius, length, width, height, translation);
		if (!rank)
			cout << "length: "<< length << " width: " << width << " height: "<< height << "\n";
		/* sort atoms in the list */

		model.atomsInModel.sort(PDBModel::compareAtoms);

		numberOfAtoms = model.atomsInModel.size();

		if (rank < procs - 1)
			atomsPerSlice = numberOfAtoms / numberOfSlices;
		else
			atomsPerSlice = numberOfAtoms - (numberOfSlices - 1) * (numberOfAtoms / numberOfSlices);

		int atomCounter = rank * (numberOfAtoms / numberOfSlices);
		list<atom>::iterator it = model.atomsInModel.begin();
		while (it != model.atomsInModel.end() && atomCounter > 0) {
			++it;
			--atomCounter;
		}
		atomCounter = atomsPerSlice;
		float minX, maxX;
		minX = it->x;
		while (it != model.atomsInModel.end() && atomCounter > 0) {
			++it;
			--atomCounter;
		}
		--it;
		maxX = it->x;
		if (!rank && procs > 1)
			sliceLength = (uint16_t)ceil(maxX * resolution + translation.x) + 1;
		else if (rank < procs - 1)
			sliceLength = (uint16_t)ceil(maxX * resolution + translation.x) -
					(uint16_t)ceil(minX * resolution + translation.x) + 1;
		else if (procs > 1)
			sliceLength = length - (uint16_t)ceil(minX * resolution + translation.x) + 1;
		else
			sliceLength = length;

		margin = 0;
		if (numberOfSlices > 1) {
			/* margin cannot be 0 */
			margin = 1;

			if (surfType == 3) {
				/* The margin must be increased by ceil(probeRadius * resolution)
				 * in order to get correct borders, and by another + 1 in order to
				 * correctly identify the cavities. */
				margin += (uint16_t)ceil(probeRadius * resolution) + 1;
			}
			if (sliceLength < 1) {
				stringstream ss;
				ss << "VoxSurf::main() - ";
				ss << "Invalid slice length value: the slice length is smaller than 1.";
				ss << "Try setting a higher \"resolution\" value.";
				throw invalid_argument(ss.str());
			}

			if (rank > 0) {
				translation.x -= (uint16_t)ceil(minX * resolution + translation.x);
				translation.x += margin;
			}
		}
		if (!rank)
			cout << "***************************************************\n";

		/* Blocks until all processes in the communicator have reached this routine. */
		MPI_Barrier(MPI_COMM_WORLD);
		/* Returns the time in seconds since an arbitrary time in the past. */
		double start = MPI_Wtime();

		cout <<"Process "<< rank << ": Calculating the surface for slice " << rank + 1 <<"\n";



		if (!rank || rank == procs - 1)
			molSurface = new MolecularSurface(&model.atomsInModel, probeRadius, resolution, onlyHeavy,
					addProbeRadius, sliceLength + margin, width, height,
					translation, rank);
		else
			molSurface = new MolecularSurface(&model.atomsInModel, probeRadius, resolution, onlyHeavy,
					addProbeRadius, sliceLength + 2 * margin, width, height,
					translation, rank);
		cout <<"Process "<< rank << ": Creating the space-filling model\n";
		molSurface->createCPKModel();
		molSurface->buildSurface();

		if (surfType == 3) {
			if (algorithmType == 1) {
				cout << "Process " << rank
						<< ": Running the rolling sphere algorithm\n";
				molSurface->rollingSphere();
			} else if (algorithmType == 2) {
				cout << "Process " << rank
						<< ": Calculating the Euclidean Distance Transform using \n";
				cout << "space-optimized data structures\n";
				molSurface->regionGrowingEDT();
			} else if (algorithmType == 3) {
				cout << "Process " << rank
						<< ": Calculating the Euclidean Distance Transform using \n";
				cout << "speed-optimized data structures\n";
				molSurface->fastRegionGrowingEDT();
			}
		}
#ifndef NO_OUTPUT_TEST
		if (binvox) {
			/* Output binvox model to file */
			cout << "Output binvox model\n";
			molSurface->outputBinvoxModel(outname);
		} else {
			/* Output PCD model to file */
			cout << "Process " << rank << ": Output PCD model\n";
			stringstream ss;
			ss << "";
			if (numberOfSlices > 1)
				ss << "-" << rank + 1;
			molSurface->outputSurfacePCDModel(outname + ss.str(), margin);
//			molSurface->outputSurfaceGrid(outname + ss.str(), margin);
		}
#endif
		if (surfType == 3 && numberOfSlices > 1) {
			cout << "Process " << rank << ": Extracting candidate pockets from slice "<< rank + 1 <<  ".\n";
			molSurface->extractPocketVoxels(&candidatePockets, margin);
			cout << "Process " << rank
					<< ": Number of candidate pockets extracted: "
					<< candidatePockets.size() << "\n";
		}
		delete molSurface;
		MPI_Datatype MPI_VOXEL;
		MPI_Type_contiguous(3, MPI_UNSIGNED_SHORT, &MPI_VOXEL); /* Creates a contiguous datatype */
		MPI_Type_commit(&MPI_VOXEL); /* Commits the datatype */

		if (surfType == 3 && numberOfSlices > 1) {
			if(!rank) {
				cout << "***************************************************\n";
				cout << "Checking candidate pockets if solvent accessible\n";
			}
			int send = 1;
			int resolved = 0;
			while(send > 0) {

			uint32_t myNumCandidates = candidatePockets.size();
			voxel* myCandidatesLeft = NULL;
			voxel* myCandidatesRight = NULL;

			uint32_t leftSliceNumCandidates = 0;
			voxel* leftSliceCandidates = NULL;

			uint32_t rightSliceNumCandidates = 0;
			voxel* rightSliceCandidates = NULL;

			if (myNumCandidates > 0) {
				if (rank > 0)
					myCandidatesLeft = new voxel[myNumCandidates];
				if (rank < procs -1)
					myCandidatesRight = new voxel[myNumCandidates];
			}
			/* if not the last process, send the number of candidate pockets to the next process */
			if (rank < procs - 1)
				MPI_Send(&myNumCandidates, 1, MPI_UNSIGNED, rank + 1, 0, MPI_COMM_WORLD);
			/* if not the first process, receive the number of candidate pockets from the previous process */
			if (rank > 0)
				MPI_Recv(&leftSliceNumCandidates, 1, MPI_UNSIGNED, rank - 1, 0, MPI_COMM_WORLD, &status);
			if (leftSliceNumCandidates > 0)
				leftSliceCandidates = new voxel[leftSliceNumCandidates];
			/* if not the first process, send the number of candidate pockets to the previous process */
			if (rank > 0)
				MPI_Send(&myNumCandidates, 1, MPI_UNSIGNED, rank - 1, 0, MPI_COMM_WORLD);
			/* if not the last process, receive the number of candidate pockets from the next process */
			if (rank < procs - 1)
				MPI_Recv(&rightSliceNumCandidates, 1, MPI_UNSIGNED, rank + 1, 0, MPI_COMM_WORLD, &status);
			if (rightSliceNumCandidates > 0)
				rightSliceCandidates = new voxel[rightSliceNumCandidates];


			for (int ii = 0; ii < procs - 1; ++ii) {
				if (rank == ii) {
					list<Pocket>::iterator it = candidatePockets.begin();
					for (int i = 0; i < myNumCandidates; i++) {
						if (!(it->rightBorder.empty()) && !(it->isPocket))
							myCandidatesRight[i] = *(it->rightBorder.begin());
						else
							myCandidatesRight[i] = voxel(0,0,0);
						++it;
					}
					if (myNumCandidates > 0)
						MPI_Send(myCandidatesRight, myNumCandidates, MPI_VOXEL, rank + 1, 0, MPI_COMM_WORLD);
					if (rightSliceNumCandidates > 0)
						MPI_Recv(rightSliceCandidates, rightSliceNumCandidates, MPI_VOXEL, rank + 1, 0, MPI_COMM_WORLD, &status);

					list<Pocket> rightCandidates;
					for (int i = 0; i < rightSliceNumCandidates; ++i) {
						Pocket temp(0, 0);
						temp.leftBorder.push_back(rightSliceCandidates[i]);
						rightCandidates.push_back(temp);
					}
					Pocket::matchPocketSlices(candidatePockets, rightCandidates);
				}
				if (rank == ii + 1) {
					list<Pocket>::iterator it = candidatePockets.begin();
					for (int i = 0; i < myNumCandidates; i++) {
						if (!(it->leftBorder.empty()) && !(it->isPocket))
							myCandidatesLeft[i] = *(it->leftBorder.begin());
						else
							myCandidatesLeft[i] = voxel(0,0,0);
						++it;
					}
					if (myNumCandidates > 0)
						MPI_Send(myCandidatesLeft, myNumCandidates, MPI_VOXEL, rank - 1, 0, MPI_COMM_WORLD);
					if (leftSliceNumCandidates > 0)
						MPI_Recv(leftSliceCandidates, leftSliceNumCandidates, MPI_VOXEL, rank - 1, 0, MPI_COMM_WORLD, &status);

					list<Pocket> leftCandidates;
					for (int i = 0; i < leftSliceNumCandidates; ++i) {
						Pocket temp(0,0);
						temp.rightBorder.push_back(leftSliceCandidates[i]);
						leftCandidates.push_back(temp);
					}
					Pocket::matchPocketSlices(leftCandidates, candidatePockets);
				}
			}

			for (int ii = procs - 2; ii >= 0; --ii) {
				if (rank == ii) {
					list<Pocket>::iterator it = candidatePockets.begin();
					for (int i = 0; i < myNumCandidates; i++) {
						if (!(it->rightBorder.empty()) && !(it->isPocket))
							myCandidatesRight[i] = *(it->rightBorder.begin());
						else
							myCandidatesRight[i] = voxel(0,0,0);
						++it;
					}
					if (myNumCandidates > 0)
						MPI_Send(myCandidatesRight, myNumCandidates, MPI_VOXEL, rank + 1, 0, MPI_COMM_WORLD);
					if (rightSliceNumCandidates > 0)
						MPI_Recv(rightSliceCandidates, rightSliceNumCandidates, MPI_VOXEL, rank + 1, 0, MPI_COMM_WORLD, &status);

					list<Pocket> rightCandidates;
					for (int i = 0; i < rightSliceNumCandidates; ++i) {
						Pocket temp(0, 0);
						temp.leftBorder.push_back(rightSliceCandidates[i]);
						rightCandidates.push_back(temp);
					}
					Pocket::matchPocketSlices(candidatePockets, rightCandidates);
				}
				if (rank == ii + 1) {
					list<Pocket>::iterator it = candidatePockets.begin();
					for (int i = 0; i < myNumCandidates; i++) {
						if (!(it->leftBorder.empty()) && !(it->isPocket))
							myCandidatesLeft[i] = *(it->leftBorder.begin());
						else
							myCandidatesLeft[i] = voxel(0,0,0);
						++it;
					}
					if (myNumCandidates > 0)
						MPI_Send(myCandidatesLeft, myNumCandidates, MPI_VOXEL, rank - 1, 0, MPI_COMM_WORLD);
					if (leftSliceNumCandidates > 0)
						MPI_Recv(leftSliceCandidates, leftSliceNumCandidates, MPI_VOXEL, rank - 1, 0, MPI_COMM_WORLD, &status);

					list<Pocket> leftCandidates;
					for (int i = 0; i < leftSliceNumCandidates; ++i) {
						Pocket temp(0,0);
						temp.rightBorder.push_back(leftSliceCandidates[i]);
						leftCandidates.push_back(temp);
					}
					Pocket::matchPocketSlices(leftCandidates, candidatePockets);

				}
			}
			int resolved_pockets = 0;
			for (auto const & cp : candidatePockets) {
				if (cp.isPocket)
					++resolved_pockets;
			}
			int difference = resolved_pockets - resolved;
			if (resolved_pockets > resolved)
				resolved = resolved_pockets;

			/**
			 * If at least one slice did identify one or more candidates as real
			 * pockets, repeat one more iteration of the algorithm.
			 * The algorithm stops if no new pockets are identified.
			 */
			MPI_Allreduce(&difference, &send, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

			if (myCandidatesLeft != NULL)
				delete [] myCandidatesLeft;
			if (myCandidatesRight != NULL)
				delete [] myCandidatesRight;
			if (leftSliceCandidates != NULL)
				delete [] leftSliceCandidates;
			if (rightSliceCandidates != NULL)
				delete [] rightSliceCandidates;
		}
			MPI_Type_free(&MPI_VOXEL);


#ifndef NO_OUTPUT_TEST
			stringstream ss;
			ss << "-pockets-" << (rank + 1);
			Pocket::outputPocketPCDModel(outname + ss.str(), candidatePockets, resolution, translation);
#endif
		}

		MPI_Barrier(MPI_COMM_WORLD); /* stop all processes */
		if(!rank) {
			cout << "**************************************************\n";
			cout << "Surface calculation time:\t" << MPI_Wtime() - start << "\n";
			cout << "Surface calculation completed successfully!\n" << endl;
		}
		MPI_Finalize();
		return EXIT_SUCCESS;
	}  catch (ParsingPDBException &e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	} catch (fstream::failure &e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	} catch (out_of_range &e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	} catch (invalid_argument &e) {
		if (!rank)
			cerr << "error: " << e.what() << "\n";
		MPI_Finalize();
		return EXIT_FAILURE;
	}
}

