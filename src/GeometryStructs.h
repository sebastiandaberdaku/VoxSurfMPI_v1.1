/*
 * GeometryStructs.h
 *
 *  Created on: Dec 23, 2013
 *      Author: daberdaku
 */
/**
 * This header file contains different geometry-related
 * type definitions.
 */

#ifndef GEOMETRYSTRUCTS_H_
#define GEOMETRYSTRUCTS_H_

#include <map>
#include <math.h>
#include <stdexcept>
#include <stdint.h>
#include <queue>
#include <iostream>

/**
 * Struct defining a voxel data type.
 * A single voxel is univocally identified by it's coordinates.
 */
//#pragma pack(push)  /* push current alignment to stack */
//#pragma pack(1)     /* set alignment to 1 byte boundary */
typedef struct voxel {
	voxel() : ix(0), iy(0), iz(0) { };
	voxel(uint16_t i, uint16_t j, uint16_t k) :
		ix(i), iy (j), iz(k) { };
	voxel(voxel const & vox) :
		ix(vox.ix), iy (vox.iy), iz(vox.iz) { };
	uint16_t ix, iy, iz;

	inline uint16_t sDistance_to(voxel const& x) const {
		int32_t dx, dy, dz;
		dx = ix - x.ix;
		dy = iy - x.iy;
		dz = iz - x.iz;
		return (dx*dx + dy*dy + dz*dz);
	}
} voxel;
//#pragma pack(pop)   /* restore original alignment from stack */


/**
 * Struct defining a distanceMapValue data type.
 */
//#pragma pack(push)  /* push current alignment to stack */
//#pragma pack(1)     /* set alignment to 1 byte boundary */
typedef struct distanceMapValue {
	distanceMapValue ( ) : sDistance(0), nearestSurfVox(voxel(0, 0, 0)) { };
	distanceMapValue (distanceMapValue const & value) :
		sDistance(value.sDistance), nearestSurfVox(value.nearestSurfVox) { };
	distanceMapValue (uint16_t sDistance, voxel nearestSurfVox) :
		sDistance(sDistance), nearestSurfVox(nearestSurfVox) { };
	uint16_t sDistance; /**< Squared distance from the nearest boundary/surface voxel. */
	voxel nearestSurfVox; /**< The nearest surface voxel. */
} distanceMapValue;
//#pragma pack(pop)   /* restore original alignment from stack */

/**
 * Struct defining a partial Euclidean Distance Map data type.
 * This data type serves as a container for a map data type.
 */
typedef struct partialEDMap {
	/**
	 * This method inserts the input <key, value> couple into the
	 * distance map. If another couple with the same key exists,
	 * the method replaces it.
	 * \param key		The voxel corresponding to the current distanceMapValue
	 * \param value		The distanceMapValue for the current voxel.
	 */
	void insert(voxel const & key, distanceMapValue const & value) {
		std::map<voxel, distanceMapValue, cmp_voxel>::iterator it;
		/* Returns an iterator to the couple with the given key if it exists,
		 * or to map.end() if no existing correspondence is found. */
		it = distanceMap.find(key);
		/* Remove the existing entry if its distance
		 * value is greater than the current one. */
		if (it != distanceMap.end()) {
			distanceMap.erase(it);
		}
		distanceMap.insert(std::pair<voxel, distanceMapValue>(key, value));
	};
	/**
	 * Simple find method. Returns a pointer to the corresponding distance
	 * map value if an entry with the given key already exists in the map,
	 * NULL otherwise.
	 * \param key 	The key of the desired distanceMapValue.
	 * \return		A reference to the desired distanceMapValue if it
	 * 				exists, NULL otherwise.
	 */
	distanceMapValue* find(voxel const & key) {
		std::map<voxel, distanceMapValue, cmp_voxel>::iterator it;
		it = distanceMap.find(key);
		if (it == distanceMap.end())
			return NULL;
		return &(it->second);
	}
	/**
	 * Comparator for the voxel data type.
	 */
	struct cmp_voxel {
		bool operator()(voxel const & a, voxel const & b) {
			if (a.ix != b.ix)
				return (a.ix < b.ix);
			else if (a.iy != b.iy)
				return (a.iy < b.iy);
			else
				return (a.iz < b.iz);
		}
	};
	std::map<voxel, distanceMapValue, cmp_voxel> distanceMap;
} partialEDMap;

/**
 * Determine the word size from the preprocessor defines.
 */
#if defined WORD32
/** 32-bit CPU word */
typedef uint32_t word_t;
#else
/** 64-bit CPU word */
typedef uint64_t word_t;
#endif

/**
 * Struct defining the voxelized representation of the molecule.
 */
typedef struct voxelGrid {
public:
	/**
	 * Constructor of the voxelGrid object.
	 *
	 * \param length			The length of the voxel grid.
	 * \param width				The width of the voxel grid.
	 * \param height			The height of the voxel grid.
	 */
	voxelGrid(uint16_t length, uint16_t width, uint16_t height);
	/**
	 * Copy constructor of the voxelGrid object. Creates a new copy of
	 * an existing voxelGrid.
	 *
	 * \param grid	The voxelGrid to copy.
	 */
	voxelGrid(voxelGrid const & grid);
	/**
	 * Method for setting the elements of the grid by their 3D coordinates.
	 * \param i		The x coordinate of the voxel to be accessed.
	 * \param j		The y coordinate of the voxel to be accessed.
	 * \param k		The z coordinate of the voxel to be accessed.
	 */
	void setVoxel(int32_t const & i, int32_t const & j, int32_t const & k);
	/**
	 * Method for clearing the elements of the grid by their 3D coordinates.
	 * \param i		The x coordinate of the voxel to be accessed.
	 * \param j		The y coordinate of the voxel to be accessed.
	 * \param k		The z coordinate of the voxel to be accessed.
	 */
	void clearVoxel(int32_t const & i, int32_t const & j, int32_t const & k);
	/**
	 * Method for reading the elements of the grid by their 3D coordinates.
	 * \param i		The x coordinate of the voxel to be accessed.
	 * \param j		The y coordinate of the voxel to be accessed.
	 * \param k		The z coordinate of the voxel to be accessed.
	 */
	bool getVoxel(int32_t const & i, int32_t const & j, int32_t const & k);
	/** Sets all the elements of the grid to false. */
	void clear_all();
	/** Sets all the elements of the grid to true. */
	void set_all();
	friend std::ostream& operator <<(std::ostream& s, const voxelGrid& grid);
	/**
	 * Destructor of the voxelGrid object.
	 */
	virtual ~voxelGrid();
	uint16_t length, width, height;
	uint64_t size;
	word_t *flags;	/**< Each voxel is composed of a boolean flag. */
private:
	/**
	 * Returns the block index of the voxel with the given indexes.
	 * \param i		ix coordinate of the desired voxel
	 * \param j		iy coordinate of the desired voxel
	 * \param k		iz coordinate of the desired voxel
	 * \return 		the block index of the desired voxel
	 */
	uint64_t getBlockIndex(uint16_t const & i, uint16_t const & j, uint16_t const & k);
	/**
	 * Returns the offset value of the voxel within the block.
	 * \param i		ix coordinate of the desired voxel
	 * \param j		iy coordinate of the desired voxel
	 * \param k		iz coordinate of the desired voxel
	 * \return 		the offset value
	 */
	uint64_t getOffset(uint16_t const & i, uint16_t const & j, uint16_t const & k);
	static const word_t one;
	static const word_t zeros;
	static const uint16_t wordSize;
} voxelGrid;

/**
 * Struct defining a 3D point data type.
 * A point3D identifies a single point in the 3D Cartesian space.
 */
typedef struct point3D {
	float x, y, z;
} point3D;

typedef struct sphereCoord {
	sphereCoord(int32_t i, int32_t j, int32_t k) : i(i), j(j), k(k) {};
	int32_t i, j, k;
} sphereCoord;

typedef struct HierarchicalQueue {
public:
	HierarchicalQueue(uint16_t numberOfQueues) : numberOfQueues(numberOfQueues) {
		queues = new std::queue<voxel>[numberOfQueues];
	};
	virtual ~HierarchicalQueue() {
		delete [] queues;
	}
	void pop() {
		bool found = false;
		int ii = 0;
		while (!found && ii < numberOfQueues) {
			if (!queues[ii].empty()) {
				queues[ii].pop();
				found = true;
			}
			else
				++ii;
		}
	};
	size_t size() {
		size_t num = 0;
		for (int i = 0; i < numberOfQueues; ++i) {
			num += queues[i].size();
		}
		return num;
	};
	bool empty() {
		return size() == 0;
	};
	voxel front() {
		voxel vox;
		bool found = false;
		int ii = 0;
		while (!found && ii < numberOfQueues) {
			if (!queues[ii].empty()) {
				vox = queues[ii].front();
				found = true;
			}
			else
				++ii;
		}
		return vox;
	};
	std::queue<voxel>& operator[] (size_t idx) {
#if defined RANGECHECK_TEST
		if (idx >= numberOfQueues)
			throw std::out_of_range("HierarchicalQueue::operator[] - Index is out of bounds!");
#endif
		return queues[idx];
	};
	const std::queue<voxel>& operator[] (size_t idx) const {
#if defined RANGECHECK_TEST
		if (idx >= numberOfQueues)
			throw std::out_of_range("HierarchicalQueue::operator[] - Index is out of bounds!");
#endif
		return queues[idx];
	};
	void push(voxel vox, uint16_t idx) {
#if defined RANGECHECK_TEST
		if (idx >= numberOfQueues)
			throw std::out_of_range("HierarchicalQueue::push() - Index is out of bounds!");
#endif
		queues[idx].push(vox);
	};
private:
	uint16_t numberOfQueues;
	std::queue<voxel>* queues;

} HierarchicalQueue;

#endif /* GEOMETRYSTRUCTS_H_ */
