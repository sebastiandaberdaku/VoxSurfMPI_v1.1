/*
 * GeometryStructs.cpp
 *
 *  Created on: Apr 15, 2014
 *      Author: sebastian
 */

#include "GeometryStructs.h"

const word_t voxelGrid::one = 1;
const word_t voxelGrid::zeros = 0;
const uint16_t voxelGrid::wordSize = 8 * sizeof(word_t);

/**
 * Constructor of the voxelGrid object.
 *
 * \param length			The length of the voxel grid.
 * \param width				The width of the voxel grid.
 * \param height			The height of the voxel grid.
 */
voxelGrid::voxelGrid(uint16_t length, uint16_t width, uint16_t height) :
		length(length), width(width), height(height) {
	size = length * width * height / wordSize + 1;
	flags = new word_t[size];
	clear_all();
}
/**
 * Copy constructor of the voxelGrid object. Creates a new copy of
 * an existing voxelGrid.
 *
 * \param grid	The voxelGrid to copy.
 */
voxelGrid::voxelGrid(voxelGrid const & grid) :
		length(grid.length), width(grid.width), height(grid.height), size(
				grid.size) {
	flags = new word_t[size]();
	std::copy(grid.flags, grid.flags + grid.size, this->flags);
}

/**
 * Method for setting the elements of the grid by their 3D coordinates.
 * \param i		The x coordinate of the voxel to be accessed.
 * \param j		The y coordinate of the voxel to be accessed.
 * \param k		The z coordinate of the voxel to be accessed.
 */
void voxelGrid::setVoxel(int32_t const & i, int32_t const & j,
		int32_t const & k) {
#if defined RANGECHECK_TEST
	if (i < 0 || i >= length)
		throw std::out_of_range(
				"voxelGrid::setVoxel() - Index i is out of bounds!");
	if (j < 0 || j >= width)
		throw std::out_of_range(
				"voxelGrid::setVoxel() - Index j is out of bounds!");
	if (k < 0 || k >= height)
		throw std::out_of_range(
				"voxelGrid::setVoxel() - Index k is out of bounds!");
#endif
	flags[getBlockIndex(i, j, k)] |= (one << getOffset(i, j, k));

}

/**
 * Method for clearing the elements of the grid by their 3D coordinates.
 * \param i		The x coordinate of the voxel to be accessed.
 * \param j		The y coordinate of the voxel to be accessed.
 * \param k		The z coordinate of the voxel to be accessed.
 */
void voxelGrid::clearVoxel(int32_t const & i, int32_t const & j,
		int32_t const & k) {
#if defined RANGECHECK_TEST
	if (i < 0 || i >= length)
		throw std::out_of_range(
				"voxelGrid::clearVoxel() - Index i is out of bounds!");
	if (j < 0 || j >= width)
		throw std::out_of_range(
				"voxelGrid::clearVoxel() - Index j is out of bounds!");
	if (k < 0 || k >= height)
		throw std::out_of_range(
				"voxelGrid::clearVoxel() - Index k is out of bounds!");
#endif
	flags[getBlockIndex(i, j, k)] &= ~(one << getOffset(i, j, k));

}

/**
 * Method for reading the elements of the grid by their 3D coordinates.
 * \param i		The x coordinate of the voxel to be accessed.
 * \param j		The y coordinate of the voxel to be accessed.
 * \param k		The z coordinate of the voxel to be accessed.
 */
bool voxelGrid::getVoxel(int32_t const & i, int32_t const & j,
		int32_t const & k) {
#if defined RANGECHECK_TEST
	if (i < 0 || i >= length)
		throw std::out_of_range(
				"voxelGrid::getVoxel() - Index i is out of bounds!");
	if (j < 0 || j >= width)
		throw std::out_of_range(
				"voxelGrid::getVoxel() - Index j is out of bounds!");
	if (k < 0 || k >= height)
		throw std::out_of_range(
				"voxelGrid::getVoxel() - Index k is out of bounds!");
#endif
	return (flags[getBlockIndex(i, j, k)] & (one << getOffset(i, j, k)))
			!= zeros;
}

/** Sets all the elements of the grid to false. */
void voxelGrid::clear_all() {
	std::fill(flags, flags + size, zeros);
}

/** Sets all the elements of the grid to true. */
void voxelGrid::set_all() {
	std::fill(flags, flags + size, ~zeros);
}
std::ostream& operator <<(std::ostream& s, const voxelGrid& grid) {
	for (uint64_t i = 0; i < grid.size; ++i) {
		s << grid.flags[i];
	}
	return s;
}
/**
 * Destructor of the voxelGrid object.
 */
voxelGrid::~voxelGrid() {
	delete[] flags;
}

uint16_t length, width, height;
uint64_t size;
word_t *flags; /**< Each voxel is composed of a boolean flag. */
uint64_t idx;
/**
 * Returns the block index of the voxel with the given indexes.
 * \param i		ix coordinate of the desired voxel
 * \param j		iy coordinate of the desired voxel
 * \param k		iz coordinate of the desired voxel
 * \return 		the block index of the desired voxel
 */
uint64_t voxelGrid::getBlockIndex(uint16_t const & i, uint16_t const & j,
		uint16_t const & k) {
	idx = i * width * height + j * height + k;
	return idx / wordSize;
}

/**
 * Returns the offset value of the voxel within the block.
 * \param i		ix coordinate of the desired voxel
 * \param j		iy coordinate of the desired voxel
 * \param k		iz coordinate of the desired voxel
 * \return 		the offset value
 */
uint64_t voxelGrid::getOffset(uint16_t const & i, uint16_t const & j,
		uint16_t const & k) {
	idx = i * width * height + j * height + k;
	return idx % wordSize;
}

