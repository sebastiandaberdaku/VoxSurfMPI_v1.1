/*
 * MolecularSurface.cpp
 *
 *  Created on: Dec 23, 2013
 *      Author: daberdaku
 */
/**
 * Implementation of the MolecularSurface class. This class
 * defines the molecular surface object, with all the methods
 * needed for it's calculation. The aim is to calculate a
 * voxelized representation for the different molecular surfaces:
 * i.e. van der Waals, Solvent Accessible and Solvent Excluded.
 */
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <iostream>
#include <fstream>
#include <sstream>
#include "atom_radii.h"
#include "param.h"
#include "MolecularSurface.h"
#include "ParsingPDBException.h"
#include "Pocket.h"
/**
 * Constructor of the class. Initializes the voxel grid and other data structures.
 *
 * \param atomList			List containing the atoms of the molecule.
 * \param probeRadius		Desired probe radius of the solvent rolling sphere.
 * \param resolution		resolution^3 = number of voxels per Å^3
 * \param onlyHeavy			True: heavy atoms only;
 * 							false: all atoms (i.e. also Hydrogen).
 * 							X-ray crystallography cannot resolve hydrogen atoms in
 * 							most protein crystals, so in most PDB files, hydrogen
 * 							atoms are absent. Sometimes hydrogens are added by
 * 							modeling. Hydrogens are always present in PDB files
 * 							resulting from NMR analysis, and usually present in
 * 							theoretical models.
 * \param addProbeRadius	true: add the probe radius to the atomic radius
 * 							values, SAS and MS (SES) have this value set to true;
 * 							false: use original atomic radius values.
 * \param length			Length of the voxel grid.
 * \param width				Width of the voxel grid.
 * \param height			Height of the voxel grid.
 * \param translation		Translation vector to the grid's coordinate system
 * 							(already scaled).
 *
 */
MolecularSurface::MolecularSurface(std::list<atom>* atomList, float probeRadius,
		float resolution, bool onlyHeavy, bool addProbeRadius, uint16_t length,
		uint16_t width, uint16_t height, point3D translation, int rank) :
		atomList(atomList), probeRadius(probeRadius), resolution(resolution),
		onlyHeavy(onlyHeavy), addProbeRadius(addProbeRadius), plength(length),
		pwidth(width), pheight(height), ptran(translation), rank(rank) {

	/*
	 * Create the voxelized box data-structure which will contain
	 * the molecule to elaborate.
	 */
	cpkModel = new voxelGrid(length, width, height);
	surface = new voxelGrid(length, width, height);
	temp = new voxelGrid(length, width, height);

	/*
	 * Calculate the cutoff value for the EDT algorithm.
	 */
	cutradis = probeRadius * resolution;
	s_cutradis = pow(cutradis, 2);

} /* MolecularSurface() */

/**
 * Destructor of the class.
 */
MolecularSurface::~MolecularSurface() {
	delete cpkModel;
	delete surface;
	delete temp;
} /* ~MolecularSurface() */

/** Calculates a bounding box containing the molecule. This method
 * calculates the maximal and minimal coordinates reached by the
 * molecule by checking all the coordinates of the atoms composing it.
 *
 * \param atomsInModel		List containing all the model's atoms.
 * \param probeRadius		Desired probe radius of the solvent rolling sphere.
 * \param resolution		resolution^3 = number of voxels per Å^3
 * \param onlyHeavy			True: heavy atoms only; false: all atoms (also Hydrogen).
 * 							X-ray crystallography cannot resolve hydrogen atoms in
 * 							most protein crystals, so in most PDB files, hydrogen
 * 							atoms are absent. Sometimes hydrogens are added by
 * 							modeling. Hydrogens are always present in PDB files
 * 							resulting from NMR analysis, and usually present in
 * 							theoretical models.
 * \param addProbeRadius	true: add the probe radius to the atomic radius
 * 							values, SAS and MS (SES) have this value set to true;
 * 							false: use original atomic radius values.
 * \param length			(return value) Pointer to the length of the voxel grid.
 * \param width				(return value) Pointer to the width of the voxel grid.
 * \param height			(return value) Pointer to the height of the voxel grid.
 * \param translation		(return value) Pointer to the translation vector to the
 * 							grid's coordinate system (already scaled).
 */
void MolecularSurface::boundingBox(std::list<atom> const & atomsInModel,
		float probeRadius, float resolution, bool onlyHeavy, bool addProbeRadius,
		uint16_t & length, uint16_t & width, uint16_t & height, point3D & translation) {
	if (atomsInModel.empty())
		throw ParsingPDBException("No atoms in PDB model.",
				"MolecularSurface::boundingBox", "Incorrect PDB file format.");
	point3D minp = { std::numeric_limits<float>::max(),
			std::numeric_limits<float>::max(),
			std::numeric_limits<float>::max() };
	point3D maxp = {std::numeric_limits<float>::min(),
			std::numeric_limits<float>::min(),
			std::numeric_limits<float>::min()};
	for (std::list<atom>::const_iterator atm = atomsInModel.begin();
			atm != atomsInModel.end(); ++atm) {
		if (atm->insertion_residue_code == ' ') {
			if (onlyHeavy && atm->element[2] == 'H')
				continue;
			if (atm->x < minp.x)
				minp.x = atm->x;
			if (atm->y < minp.y)
				minp.y = atm->y;
			if (atm->z < minp.z)
				minp.z = atm->z;
			if (atm->x > maxp.x)
				maxp.x = atm->x;
			if (atm->y > maxp.y)
				maxp.y = atm->y;
			if (atm->z > maxp.z)
				maxp.z = atm->z;
		}
	}
	float fmargin = 0;
	for (int ii = 0; ii < 13; ++ii)
		if (rasrad[ii] > fmargin)
			fmargin = rasrad[ii];
	fmargin += 0.5;

	minp.x -= fmargin;
	minp.y -= fmargin;
	minp.z -= fmargin;
	maxp.x += fmargin;
	maxp.y += fmargin;
	maxp.z += fmargin;
	if (addProbeRadius) {
		minp.x -= probeRadius;
		minp.y -= probeRadius;
		minp.z -= probeRadius;
		maxp.x += probeRadius;
		maxp.y += probeRadius;
		maxp.z += probeRadius;
	}
	/* transformation values */
	translation.x = -minp.x * resolution;
	translation.y = -minp.y * resolution;
	translation.z = -minp.z * resolution;
	/* bounding box dimensions */
	double boxLength = ceil(resolution * (maxp.x - minp.x));
	double boxWidth = ceil(resolution * (maxp.y - minp.y));
	double boxHeight = ceil(resolution * (maxp.z - minp.z));
	if (boxLength <= UINT16_MAX
			&& boxWidth <= UINT16_MAX
			&& boxHeight <= UINT16_MAX) {
		length = uint16_t(boxLength);
		width = uint16_t(boxWidth);
		height = uint16_t(boxHeight);
	}
	else {
		std::stringstream ss;
		ss << "MolecularSurface::boundingBox() - ";
		ss << "The bounding box's dimensions exceed the maximum value of " << UINT16_MAX << ". ";
		ss << "Try setting a lower \"resolution\" value.";
		throw std::invalid_argument(ss.str());

	}
} /* boundingBox() */

/** Build the boundary of the solid created with the space-filling algorithm.
 * A voxel belongs to the boundary if it has at least one neighbor which is not
 * occupied by any atom.
 */
void MolecularSurface::buildSurface() {
	int ii; /**< Neighbor index - each voxel has at most 26 neighbors. */
	bool flagbound; /**< Boundary switch - if an internal voxel has an external
	 * neighbor than stop checking the other neighbors. */
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				/* if the current voxel is inside the molecule */
				if (cpkModel->getVoxel(i, j, k)) {
					flagbound = false;
					ii = 0;
					/* while not a boundary voxel, and while not all of its
					 * neighbors have been checked */
					while (!flagbound && ii < 26) {
						if (k + nb[ii][2] > -1 && k + nb[ii][2] < pheight &&
								j + nb[ii][1] > -1 && j + nb[ii][1] < pwidth &&
								i + nb[ii][0] > -1 && i + nb[ii][0] < plength) {
							if (!cpkModel->getVoxel(i + nb[ii][0], j + nb[ii][1], k + nb[ii][2])) {
								surface->setVoxel(i, j, k); /* is a boundary voxel */
								/* stop checking the remaining neighbors */
								flagbound = true;
							} // if
							else {
								++ii; /* go on with the next neighbor */
							} // else if
						} // if
						else {
							++ii; /* go on with the next neighbor */
						} // else if
					} // while
				} // if
			} // for k
		} // for j
	} // for i
} /* buildSurface() */

/** Method that removes any spurious internal voids. Macromolecules
 * can have solvent-excluded cavities and voids, which might generate
 * spurious surfaces inside the real molecular surface. This method
 * calls a simple three-dimensional flood fill algorithm, which
 * "colors" the voxels on the outside of the most external protein
 * surface. The "coloring" process starts from one of the eight
 * vertices of the bounding box. Note that, if an adequately high
 * resolution is used, the voxels at the vertices of the bounding box
 * will never belong to any atom. Just think of fitting some spheres
 * inside a box: there will always be some free space at its edges.
 */
void MolecularSurface::fillInternalCavities() {
	/* Sets temp equal to cpkModel. Both variables will describe
	 * the same volume. */
	if (temp != NULL)
		delete temp;
	temp = new voxelGrid(*cpkModel);

	/* initialize */
	cpkModel->set_all();

	/* Determine the starting point for the flood fill algorithm.
	 * Start by testing an edge voxel of the bounding box.
	 * If an adequate resolution is used, the edges should not be
	 * occupied by any atom. */
	if (temp->getVoxel(0, 0, 0)) {
		/* This possibility indicates an erroneous calculation of
		 * the protein surface. */
		std::cout << "Process "<< rank << ": No adequate starting voxel found for the "
				<< "flood fill algorithm. Aborting.\n";
		return;
	}
	voxel startingVoxel(0, 0, 0);
	/* Call to the three-dimensional flood fill algorithm. The
	 * occupied variable is used to "color" the all voxels lying
	 * between the bounding box's boundaries and the molecular
	 * surface.
	 */
	floodFill3D(startingVoxel);
} /* fillInternalCavities() */

/** Fills the voxels occupied by one atom. This method creates a volumetric
 * representation for a single atom. All voxels occupied by the input atom
 * will be marked by setting the occupied field to true.
 *
 * \param atm 	Pointer to the atom.
 */
void MolecularSurface::spaceFillAtom(atom const & atm) {
	/** Discretized coordinates of the atom's center.
	 * Translate and discretize the coordinates */
	int cx = int(ceil(atm.x * resolution + ptran.x));
	int cy = int(ceil(atm.y * resolution + ptran.y));
	int cz = int(ceil(atm.z * resolution + ptran.z));

	int si, sj, sk; /**< Surrounding voxels' coordinates. */
//	int at = getRasRadIndex(atm.name.c_str()); /* Atom type. */
//	std::list<sphereCoord>::iterator it;
//	if (cx + iradius[at] < 0 || cx - iradius[at] >= plength)
//		return;
//	for (it = atomSpheres[at].begin(); it != atomSpheres[at].end(); ++it) {
//		si = cx + it->i;
//		sj = cy + it->j;
//		sk = cz + it->k;
//		if (si > -1 && si < plength
//				&& sj > -1 && sj < pwidth
//				&& sk > -1 && sk < pheight) {
//			cpkModel->setVoxel(si, sj, sk);
//		}
//	}

	auto radius = radii_CHARMM.find(res_atm(trim(atm.resname), trim(atm.name)));
	if (radius == radii_CHARMM.end() || abs(radius->second) < 1e-6) {
		cout << "Could not assign radius to atom number: " << atm.snum << " "<< atm.name << " " << atm.resname<< endl;
		return;
	}

	float r= resolution * radius->second;

	if (addProbeRadius)
		r += resolution * probeRadius;

	int irad = static_cast<int>(ceil(r));
	int srad = static_cast<int>(pow(r, 2.0) + 0.5);

	for (int i = -irad; i <= irad; ++i) {
		for (int j = -irad; j <= irad; ++j) {
			for (int k = -irad; k <= irad; ++k) {
				si = cx + i;
				sj = cy + j;
				sk = cz + k;
				if (si > -1 && si < plength
						&& sj > -1 && sj < pwidth
						&& sk > -1 && sk < pheight) {
					if (i * i + j * j + k * k <= srad)
						cpkModel->setVoxel(si, sj, sk);
				}
			} // for k
		} // for j
	} // for i
} /* spaceFillAtom() */

/** Fills the voxels in the grid occupied by the molecule (protein).
 * This method implements a space-filling algorithm which is the
 * preliminary step for our grid-based macro-molecular surface
 * generation.
 *
 * In chemistry, a space-filling model, also known as a calotte model,
 * is a type of three-dimensional molecular model where the atoms are
 * represented by spheres whose radii are proportional to the radii of
 * the atoms and whose center-to-center distances are proportional to
 * the distances between the atomic nuclei, all in the same scale.
 * Atoms of different chemical elements are usually represented by
 * spheres of different colors.
 *
 * Calotte models are distinguished from other 3D representations,
 * such as the ball-and-stick and skeletal models, by the use of "full
 * size" balls for the atoms. They are useful for visualizing the
 * effective shape and relative dimensions of the molecule, in particular
 * the region of space occupied by it. On the other hand, calotte models
 * do not show explicitly the chemical bonds between the atoms, nor the
 * structure of the molecule beyond the first layer of atoms.
 *
 * Space-filling models are also called CPK models after the chemists
 * Robert Corey, Linus Pauling and Walter Koltun, who pioneered their use.
 */
void MolecularSurface::createCPKModel() {
	/* For every atom in our list, calculate the voxels it occupies. */
	for (std::list<atom>::iterator atm = atomList->begin();
			atm != atomList->end(); ++atm) {
		if (atm->insertion_residue_code == ' ') {
			if (onlyHeavy && atm->element[2] == 'H') {
				continue;
			}
			/* Fill the voxels occupied by the current atom. */
			spaceFillAtom(*atm);
		}
	}
} /* createCPKModel() */

void MolecularSurface::rollingSphere() {
	std::list<voxel> surfaceVoxels; /* list containing the SAS voxels */
	/* for all voxels */
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				/* create the first shell by pushing the surface voxels in the queue */
				if (surface->getVoxel(i, j, k)) {
					surfaceVoxels.push_back(voxel(i, j, k));
				}
			}
		}
	}
	surface->clear_all();

	/* calculate the probe sphere's voxelized representation */
	std::list<sphereCoord> probeSphere;
	int cutoff = (int)ceil(cutradis) + 1;
	for (int i = -cutoff; i <= cutoff; ++i) {
		for (int j = -cutoff; j <= cutoff; ++j) {
			for (int k = -cutoff; k <= cutoff; ++k) {
				if (i * i + j * j + k * k < s_cutradis)
					probeSphere.push_back(sphereCoord(i, j, k));
			}
		}
	}

	std::list<voxel>::iterator surfIt;
	std::list<sphereCoord>::iterator probeIt;
	int32_t si, sj, sk; /**< Surrounding voxels' coordinates. */

	for (surfIt = surfaceVoxels.begin(); surfIt != surfaceVoxels.end(); ++surfIt) {
		for (probeIt = probeSphere.begin(); probeIt != probeSphere.end(); ++probeIt) {
			si = surfIt->ix + probeIt->i;
			sj = surfIt->iy + probeIt->j;
			sk = surfIt->iz + probeIt->k;
			if (sk > -1 && sk < pheight && sj > -1 && sj < pwidth && si > -1
					&& si < plength) {
				cpkModel->clearVoxel(si, sj, sk);
			}
		}
	}

	std::cout << "Process "<< rank << ": Filling internal cavities.\n";
	fillInternalCavities();
	std::cout << "Process "<< rank << ": Re-building surface.\n";
	buildSurface();
}

/** Calculates a partial Euclidean Distance Map of the voxelized
 * representation of the molecular surface. Starting from the Solvent-
 * Accessible Surface of the molecule, it is possible to obtain the
 * molecular surface (or Solvent-Excluded Surface) applying the EDT
 * (Euclidean Distance Transform). The SES is then extracted from the
 * partial Euclidean Distance Map, as it's voxels have a particular
 * distance value, i.e. greater or equal to the solvent/probe sphere
 * radius. The calculation starts from the boundary (surface) voxels,
 * and procedes towards the inside of the molecule, one shell at a time.
 * The initial shell is obviously the surface of the molecule. The
 * second shell is composed by voxels internal to the molecule, that
 * are neighbors to the first shell's voxels, and don't belong to the
 * first shell. The third shell will be composed by neighbors of the
 * second shell's voxels, internal to the molecule, and that don't
 * belong to the first or second shell, and so on. The distance map
 * values are propagated from the molecular surface towards the inside
 * of the molecule one shell at a time.
 */
void MolecularSurface::regionGrowingEDT() {
	partialEDMap *squaredEDMap = new partialEDMap();
	uint16_t numberOfQueues = (uint16_t) ceil(s_cutradis);
	HierarchicalQueue* HQ1 = new HierarchicalQueue(numberOfQueues);
	HierarchicalQueue* HQ2 = new HierarchicalQueue(numberOfQueues);

	temp->clear_all();
	/* for all voxels */
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				/* create the first shell by pushing the surface voxels in the list */
				if (surface->getVoxel(i, j, k)) {
					HQ1->push(voxel(i, j, k), 0);
					/* a surface voxel has zero distance from itself */
					squaredEDMap->insert(voxel(i, j, k), distanceMapValue(0, voxel(i, j, k)));
				}
			}
		}
	}
	surface->clear_all();

	while (!HQ1->empty()) {
		voxel currentVox = HQ1->front();
		HQ1->pop();
		distanceMapValue* mapValue = squaredEDMap->find(currentVox);
		voxel nearestSurfVox = mapValue->nearestSurfVox;
		uint16_t squaredDistance = mapValue->sDistance;
		int nbx, nby, nbz;
		bool isEnd = true;
		for (int i = 0; i < 26; ++i) {
			nbx = currentVox.ix + nb[i][0];
			nby = currentVox.iy + nb[i][1];
			nbz = currentVox.iz + nb[i][2];
			if (nbz < pheight && nbz > -1
					&& nby < pwidth && nby > -1
					&& nbx < plength && nbx > -1) {
				if (cpkModel->getVoxel(nbx, nby, nbz)) {
					voxel neighbour(nbx, nby, nbz);
					uint16_t newSDistance = neighbour.sDistance_to(nearestSurfVox);
					distanceMapValue* neighbourValue = squaredEDMap->find(neighbour);
					if (neighbourValue != NULL) {
						if (newSDistance < neighbourValue->sDistance && newSDistance < numberOfQueues) {
							squaredEDMap->insert(neighbour, distanceMapValue(newSDistance, nearestSurfVox));
							HQ1->push(neighbour, newSDistance);
							isEnd = false;
						}
					}
					else if (newSDistance < numberOfQueues){
						squaredEDMap->insert(neighbour, distanceMapValue(newSDistance, nearestSurfVox));
						HQ1->push(neighbour, newSDistance);
						isEnd = false;
					}
				}
			}
		}
		if (isEnd && squaredDistance >= 24) {
			HQ2->push(currentVox, squaredDistance);
		}
	}
	while (!HQ2->empty()) {
		voxel currentVox = HQ2->front();
		HQ2->pop();
		distanceMapValue* mapValue = squaredEDMap->find(currentVox);
		if (mapValue == NULL)
			continue;
		voxel nearestSurfVox = mapValue->nearestSurfVox;
		int nbx, nby, nbz;
		for (int i = -2; i <= 2; ++i) {
			for (int j = -2; j <= 2; ++j) {
				for (int k = -2; k <= 2; ++k) {
					if (i != 0 && j != 0 && k != 0) {
						nbx = currentVox.ix + i;
						nby = currentVox.iy + j;
						nbz = currentVox.iz + k;
						if (nbz < pheight && nbz > -1 && nby < pwidth
								&& nby > -1 && nbx < plength && nbx > -1) {
							if (cpkModel->getVoxel(nbx, nby, nbz)) {
								voxel neighbour(nbx, nby, nbz);
								uint16_t newSDistance = neighbour.sDistance_to(nearestSurfVox);
								distanceMapValue* neighbourValue = squaredEDMap->find(neighbour);
								if (neighbourValue != NULL) {
									if (newSDistance < neighbourValue->sDistance && newSDistance < numberOfQueues) {
										squaredEDMap->insert(neighbour, distanceMapValue(newSDistance, nearestSurfVox));
										HQ2->push(neighbour, newSDistance);
									}
								}
								else if (newSDistance < numberOfQueues) {
									squaredEDMap->insert(neighbour, distanceMapValue(newSDistance, nearestSurfVox));
									HQ2->push(neighbour, newSDistance);
								}
							}
						}
					}
				}
			}
		}
	}

	/* update the occupied flag for the remaining voxels */
	std::map<voxel, distanceMapValue, partialEDMap::cmp_voxel>::iterator it;
	for (it = squaredEDMap->distanceMap.begin(); it != squaredEDMap->distanceMap.end(); ++it) {
		if(it->second.sDistance < s_cutradis) {
			cpkModel->clearVoxel(it->first.ix, it->first.iy, it->first.iz);
		}
	}

	/* Delete the partial Euclidean Distance Map. */
	delete squaredEDMap;
	squaredEDMap = NULL;
	std::cout << "Process "<< rank << ": Filling internal cavities\n";
	fillInternalCavities();
	std::cout << "Process "<< rank << ": Re-building surface\n";
	buildSurface();
} /* regionGrowingEDT() */


void MolecularSurface::fastRegionGrowingEDT() {
	voxel ***nearestSurfaceVoxel = new voxel**[plength];
	for (int i = 0; i < plength; ++i) {
		nearestSurfaceVoxel[i] = new voxel*[pwidth];
	}
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			nearestSurfaceVoxel[i][j] = new voxel[pheight];
		}
	}
	uint16_t ***distanceMap = new uint16_t**[plength];
	for (int i = 0; i < plength; ++i) {
		distanceMap[i] = new uint16_t*[pwidth];
	}
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			distanceMap[i][j] = new uint16_t[pheight];
		}
	}

	uint16_t numberOfQueues = static_cast<uint16_t>(pow(probeRadius * resolution - sqrt(2.0), 2.0));
	HierarchicalQueue* HQ1 = new HierarchicalQueue(numberOfQueues);
	HierarchicalQueue* HQ2 = new HierarchicalQueue(numberOfQueues);

	/* for all voxels */
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				/* create the first shell by pushing the surface voxels in the list */
				if (surface->getVoxel(i, j, k)) {
					HQ1->push(voxel(i, j, k), 0);
					/* a surface voxel has zero distance from itself */
					nearestSurfaceVoxel[i][j][k] = voxel(i, j, k);
					distanceMap[i][j][k] = 0;
				}
				else {
					distanceMap[i][j][k] = UINT16_MAX;
				}
			}
		}
	}
	surface->clear_all();

	while (!HQ1->empty()) {
		voxel currentVox = HQ1->front();
		HQ1->pop();
		voxel nearestSurfVox = nearestSurfaceVoxel[currentVox.ix][currentVox.iy][currentVox.iz];
		uint16_t squaredDistance = distanceMap[currentVox.ix][currentVox.iy][currentVox.iz];;
		int nbx, nby, nbz;
		bool isEnd = true;
		for (int i = 0; i < 26; ++i) {
			nbx = currentVox.ix + nb[i][0];
			nby = currentVox.iy + nb[i][1];
			nbz = currentVox.iz + nb[i][2];
			if (nbz < pheight && nbz > -1
					&& nby < pwidth && nby > -1
					&& nbx < plength && nbx > -1) {
				if (cpkModel->getVoxel(nbx, nby, nbz)) {
					voxel neighbour(nbx, nby, nbz);
					uint16_t newSDistance = neighbour.sDistance_to(nearestSurfVox);
					if (newSDistance < distanceMap[neighbour.ix][neighbour.iy][neighbour.iz]
					                                                           && newSDistance < numberOfQueues) {
						distanceMap[neighbour.ix][neighbour.iy][neighbour.iz] = newSDistance;
						nearestSurfaceVoxel[neighbour.ix][neighbour.iy][neighbour.iz] = nearestSurfVox;
						HQ1->push(neighbour, newSDistance);
						isEnd = false;
					}
				}
			}
		}
		if (isEnd && squaredDistance >= 24) {
			HQ2->push(currentVox, squaredDistance);
		}
	}
	while (!HQ2->empty()) {
		voxel currentVox = HQ2->front();
		HQ2->pop();
		voxel nearestSurfVox = nearestSurfaceVoxel[currentVox.ix][currentVox.iy][currentVox.iz];
		int nbx, nby, nbz;
		for (int i = -2; i <= 2; ++i) {
			for (int j = -2; j <= 2; ++j) {
				for (int k = -2; k <= 2; ++k) {
					if (i != 0 && j != 0 && k != 0) {
						nbx = currentVox.ix + i;
						nby = currentVox.iy + j;
						nbz = currentVox.iz + k;
						if (nbz < pheight && nbz > -1 && nby < pwidth
								&& nby > -1 && nbx < plength && nbx > -1) {
							if (cpkModel->getVoxel(nbx, nby, nbz)) {
								voxel neighbour(nbx, nby, nbz);
								uint16_t newSDistance = neighbour.sDistance_to(nearestSurfVox);
								if (newSDistance < distanceMap[neighbour.ix][neighbour.iy][neighbour.iz]
								                                                           && newSDistance < numberOfQueues) {
									distanceMap[neighbour.ix][neighbour.iy][neighbour.iz] = newSDistance;
									nearestSurfaceVoxel[neighbour.ix][neighbour.iy][neighbour.iz] = nearestSurfVox;
									HQ2->push(neighbour, newSDistance);
								}
							}
						}
					}
				}
			}
		}
	}

	/* update the cpkModel */
	for (int i = 0; i < plength; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0; k < pheight; ++k) {
				if (distanceMap[i][j][k] < numberOfQueues) {
					cpkModel->clearVoxel(i, j, k);
				}
			}
		}
	}

	/* Delete the partial Euclidean Distance Map. */
	for (int i = 0; i < plength; i++) {
		for (int j = 0; j < pwidth; j++) {
			delete[] nearestSurfaceVoxel[i][j];
			delete[] distanceMap[i][j];
		}
	}
	for (int i = 0; i < plength; i++) {
		delete[] nearestSurfaceVoxel[i];
		delete[] distanceMap[i];
	}
	delete[] nearestSurfaceVoxel;
	nearestSurfaceVoxel = NULL;
	delete[] distanceMap;
	distanceMap = NULL;

	delete HQ1;
	HQ1 = NULL;
	delete HQ2;
	HQ2 = NULL;

	std::cout << "Process "<< rank << ": Filling internal cavities\n";
	fillInternalCavities();
	std::cout << "Process "<< rank << ": Re-building surface\n";
	buildSurface();
} /* fastRegionGrowingEDT() */

/** Simple three-dimensional flood fill algorithm. The "coloring" starts
 * from startingVoxel, and proceeds by setting the occupied variable to
 * "false" for all the voxels lying outside the molecular surface. The
 * algorithm uses an auxiliary queue data structure for storing the next
 * voxels to be "colored".
 *
 * \param startingVoxel		The starting voxel for the algorithm.
 */
void MolecularSurface::floodFill3D(voxel & startingVoxel) {
	std::queue<voxel> voxelQueue;
	voxel currentVoxel = startingVoxel;
	/* note that voxel coordinates are unsigned short, so we cannot
	 * use a voxel to keep the possible neighbor's coordinates */
	int neighborVoxelX, neighborVoxelY, neighborVoxelZ;
	voxelQueue.push(currentVoxel);
	cpkModel->clearVoxel(currentVoxel.ix, currentVoxel.iy, currentVoxel.iz);
	while (!voxelQueue.empty()) {
		currentVoxel = voxelQueue.front();
		voxelQueue.pop();
		/* for all currentVoxel's neighbors */
		for (int i = 0; i < 26; ++i) {
			/* try to fill currentVoxel's neighbors */
			neighborVoxelX = currentVoxel.ix + nb[i][0];
			neighborVoxelY = currentVoxel.iy + nb[i][1];
			neighborVoxelZ = currentVoxel.iz + nb[i][2];
			/* If not going out of bounds (i.e. if the neighbor exists),
			 * if the current neighbor has not been colored yet, and if
			 * it is not occupied by aneighborVoxelY atom, put it in the queue. */
			if (neighborVoxelZ > -1 && neighborVoxelZ < pheight
					&& neighborVoxelY > -1 && neighborVoxelY < pwidth
					&& neighborVoxelX > -1 && neighborVoxelX < plength) {
				if (cpkModel->getVoxel(neighborVoxelX, neighborVoxelY, neighborVoxelZ)
						&& !temp->getVoxel(neighborVoxelX, neighborVoxelY, neighborVoxelZ)) {
					// color it!
					cpkModel->clearVoxel(neighborVoxelX, neighborVoxelY, neighborVoxelZ);
					voxelQueue.push(voxel(neighborVoxelX, neighborVoxelY, neighborVoxelZ));
				} // if
			} // if
		} // for
	} // while
} /* floodFill3D() */

/** For the correct calculation of the molecular surface with slicing, we must detect potential
 * pocket regions on the slicing planes. A pocket region which overlaps two or more slices must
 * be handled correctly, otherwise it can end up being filled by the fillInternalCavities()
 * method. This method detects potential pocket regions on the two border planes of the current
 * slice.
 * \param pocketList	The output list of candidate pocket regions for the current slice.
 * \param margin		The margin value.
 */
void MolecularSurface::extractPocketVoxels(std::list<Pocket>* pocketList, uint16_t margin) {
	/* reset the surface flag */
	surface->clear_all();
	int nbX, nbY, nbZ;
	/*
	 * Look for potential pocket voxels. Note that, after filling the internal cavities,
	 * the occupied flag indicates all voxels inside the molecular surface, and the isDone
	 * flag indicates all voxels occupied by the space filling algorithm. Voxels on the
	 * extremities of the current slice, which have the occupied and isDone flags set to
	 * respectively true and false, belong to potential pocket areas. The voxels surrounding
	 * the pocket areas, which have the isDone flag set to true, are the ones we need to
	 * keep track of.
	 */
	for (int j = 0; j < pwidth; ++j) {
		for (int k = 0; k < pheight; ++k) {
			if (!surface->getVoxel(margin - 1, j, k)
					&& cpkModel->getVoxel(margin - 1, j, k)
					&& !temp->getVoxel(margin - 1, j, k)) {
				/* found one potential pocket */
				std::queue<voxel> voxelQueue;
				Pocket pk(margin, plength - margin - 1);
				voxel startingVoxel(margin - 1, j, k);
				voxel currentVoxel;
				surface->setVoxel(startingVoxel.ix, startingVoxel.iy,
						startingVoxel.iz);
				voxelQueue.push(startingVoxel);
				while (!voxelQueue.empty()) {
					currentVoxel = voxelQueue.front();
					voxelQueue.pop();
					for (int i = 0; i < 26; ++i) {
						nbX = currentVoxel.ix + nb[i][0];
						nbY = currentVoxel.iy + nb[i][1];
						nbZ = currentVoxel.iz + nb[i][2];
						if (nbZ > -1 && nbZ < pheight && nbY > -1
								&& nbY < pwidth && nbX >= margin - 1
								&& nbX <= plength - margin) {
							if (!temp->getVoxel(nbX, nbY, nbZ)
									&& cpkModel->getVoxel(nbX, nbY, nbZ)
									&& !surface->getVoxel(nbX, nbY, nbZ)) {
								surface->setVoxel(nbX, nbY, nbZ);
								voxelQueue.push(voxel(nbX, nbY, nbZ));
							} else if (temp->getVoxel(nbX, nbY, nbZ)
									&& cpkModel->getVoxel(nbX, nbY, nbZ)
									&& !surface->getVoxel(nbX, nbY, nbZ)) {
								surface->setVoxel(nbX, nbY, nbZ);
								if (nbX > margin - 1 && nbX < plength - margin)
									pk.pocketVoxels.push_back(
											voxel(nbX, nbY, nbZ));
							}
						}
					}
				} // while
				if (!pk.pocketVoxels.empty()) {
//					pk.calculateBorders();
					int jj, kk;
					std::list<voxel>::iterator it;
					for (it = pk.pocketVoxels.begin(); it != pk.pocketVoxels.end(); ++it) {
						if (it->ix == pk.minMargin) {
							jj = -1;
							kk = -1;
							bool border = false;
							while (jj < 2 && !border) {
								while (kk < 2 && !border) {
									if (surface->getVoxel(pk.minMargin - 1, it->iy + jj, it->iz + kk)) {
										pk.leftBorder.push_back(*it);
										border = true;
									}
									++kk;
								}
								++jj;
							}
						}
						else if (it->ix == pk.maxMargin) {
							jj = -1;
							kk = -1;
							bool border = false;
							while (jj < 2 && !border) {
								while (kk < 2 && !border) {
									if (surface->getVoxel(pk.maxMargin + 1, it->iy + jj, it->iz + kk)) {
										pk.rightBorder.push_back(*it);
										border = true;
									}
									++kk;
								}
								++jj;
							}
						}
					}
					pocketList->push_back(pk);
				}
			}
		}
	}
	for (int j = 0; j < pwidth; ++j) {
		for (int k = 0; k < pheight; ++k) {
			if (!surface->getVoxel(plength - margin, j, k)
					&& cpkModel->getVoxel(plength - margin, j, k)
					&& !temp->getVoxel(plength - margin, j, k)) {
				/* found one potential pocket */
				std::queue<voxel> voxelQueue;
				Pocket pk(margin, plength - margin - 1);
				voxel startingVoxel(plength - margin, j, k);
				voxel currentVoxel;
				surface->setVoxel(startingVoxel.ix, startingVoxel.iy,
						startingVoxel.iz);
				voxelQueue.push(startingVoxel);
				while (!voxelQueue.empty()) {
					currentVoxel = voxelQueue.front();
					voxelQueue.pop();
					for (int i = 0; i < 26; ++i) {
						nbX = currentVoxel.ix + nb[i][0];
						nbY = currentVoxel.iy + nb[i][1];
						nbZ = currentVoxel.iz + nb[i][2];
						if (nbZ > -1 && nbZ < pheight && nbY > -1
								&& nbY < pwidth && nbX >= margin - 1
								&& nbX <= plength - margin) {
							if (!temp->getVoxel(nbX, nbY, nbZ)
									&& cpkModel->getVoxel(nbX, nbY, nbZ)
									&& !surface->getVoxel(nbX, nbY, nbZ)) {
								surface->setVoxel(nbX, nbY, nbZ);
								voxelQueue.push(voxel(nbX, nbY, nbZ));
							} else if (temp->getVoxel(nbX, nbY, nbZ)
									&& cpkModel->getVoxel(nbX, nbY, nbZ)
									&& !surface->getVoxel(nbX, nbY, nbZ)) {
								surface->setVoxel(nbX, nbY, nbZ);
								if (nbX > margin - 1 && nbX < plength - margin)
									pk.pocketVoxels.push_back(
											voxel(nbX, nbY, nbZ));
							}
						}
					}
				} // while
				if (!pk.pocketVoxels.empty()) {
//					pk.calculateBorders();
					int jj, kk;
					std::list<voxel>::iterator it;
					for (it = pk.pocketVoxels.begin(); it != pk.pocketVoxels.end(); ++it) {
						if (it->ix == pk.minMargin) {
							jj = -1;
							kk = -1;
							bool border = false;
							while (jj < 2 && !border) {
								while (kk < 2 && !border) {
									if (surface->getVoxel(pk.minMargin - 1, it->iy + jj, it->iz + kk)) {
										pk.leftBorder.push_back(*it);
										border = true;
									}
									++kk;
								}
								++jj;
							}
						}
						else if (it->ix == pk.maxMargin) {
							jj = -1;
							kk = -1;
							bool border = false;
							while (jj < 2 && !border) {
								while (kk < 2 && !border) {
									if (surface->getVoxel(pk.maxMargin + 1, it->iy + jj, it->iz + kk)) {
										pk.rightBorder.push_back(*it);
										border = true;
									}
									++kk;
								}
								++jj;
							}
						}
					}
					pocketList->push_back(pk);
				}
			}
		}
	}
} /* extractPocketVoxels() */

/** Prints the 3D voxelized representation to file using the binvox
 * format.  This format uses simple compression (run-length encoding)
 * to reduce filesize. The format was put together by Michael Kazhdan.
 *
 * The binary data consists of pairs of bytes. The first byte of
 * each pair is the value byte and is either 0 or 1 (1 signifies
 * the presence of a voxel). The second byte is the count byte and
 * specifies how many times the preceding voxel value should be
 * repeated (so obviously the minimum count is 1, and the maximum
 * is 255).
 *
 * For a detailed description of the BINVOX voxel file format
 * specification see:
 * http://www.cs.princeton.edu/~min/binvox/binvox.html
 *
 * \param filename	Name of the output file. The '.binvox' extension
 * 					is added automatically.
 *
 * \throws ofstream::failure
 */
void MolecularSurface::outputBinvoxModel(std::string const &filename) {
	std::ofstream file_stream;
	file_stream.open((filename + ".binvox").c_str());
	if (!file_stream.is_open()) {
		throw std::ofstream::failure("Error opening output file.");
	}
	file_stream << "#binvox 1\n"
			<< "dim " << plength << " " << pwidth << " " << pheight << "\n"
			<< "translate 0 0 0\n"
			<< "scale " << "1" << "\n"
			<< "data\n";

	unsigned char count = 0;
	unsigned char value = surface->getVoxel(0, 0, 0);
	for (int i = 0; i < plength; ++i) { // x axis
		for (int j = 0; j < pwidth; ++j) { // y axis
			for (int k = 0; k < pheight; ++k) { // z axis
				if ((value == surface->getVoxel(i, j, k)) && (count < 255))
					++count;
				else {
					file_stream << value << count;
					value = surface->getVoxel(i, j, k);
					count = 1;
				}
			}
		}
	}
	file_stream << value << count;
	file_stream.close();
} /* outputBinvoxModel() */

/** Prints the 3D voxelized representation to file using the PCD
 * (Point Cloud Data) file format.
 *
 * Each PCD file contains a header that identifies and declares
 * certain properties of the point cloud data stored in the file.
 * The header of a PCD must be encoded in ASCII.
 * Storing point cloud data in both a simple ascii form with each
 * point on a line, space or tab separated, without any other
 * characters on it, as well as in a binary dump format, allows
 * us to have the best of both worlds: simplicity and speed,
 * depending on the underlying application. The ascii format
 * allows users to open up point cloud files and plot them using
 * standard software tools like gnuplot or manipulate them using
 * tools like sed, awk, etc.
 *
 * For a detailed description of the PCD (Point Cloud Data) file
 * format specification see:
 * http://pointclouds.org/documentation/tutorials/pcd_file_format.php
 *
 * \param filename	Name of the output file. The '.pcd' extension
 * 					is added automatically.
 * \param margin	Offset on the x axis. Only print points with the
 * 					x coordinate between margin (included) and
 * 					boxLength-margin (excluded).
 *
 * \throws ofstream::failure
 */
void MolecularSurface::outputSurfacePCDModel(std::string const & filename, uint16_t margin) {
	std::ofstream file_stream;
	file_stream.open((filename + ".pcd").c_str());
	if (!file_stream.is_open()) {
		throw std::ofstream::failure("Error opening output file.");
	}
	std::queue<voxel> surfaceVoxels;
	for (int i = margin; i < plength-margin; ++i) {
		for (int j = 0; j < pwidth; ++j) {
			for (int k = 0 ; k < pheight; ++k) {
				if (surface->getVoxel(i, j, k)) {
				surfaceVoxels.push(voxel(i, j, k));
				}
			}
		}
	}
	/* File header */
	file_stream << "# .PCD v.7 - Point Cloud Data file format\n"
			<< "VERSION .7\n"
			<< "FIELDS x y z\n"
			<< "SIZE 4 4 4\n"
			<< "TYPE F F F\n"
			<< "COUNT 1 1 1\n"
			<< "WIDTH " << surfaceVoxels.size() << "\n"
			<< "HEIGHT 1\n"
			<< "VIEWPOINT 0 0 0 1 0 0 0\n"
			<< "POINTS " << surfaceVoxels.size() << "\n"
			<< "DATA ascii";

	while (!surfaceVoxels.empty()) {
		file_stream << "\n" << (surfaceVoxels.front().ix - ptran.x)/resolution << " "
				<< (surfaceVoxels.front().iy - ptran.y)/resolution << " "
				<< (surfaceVoxels.front().iz - ptran.z)/resolution;
		surfaceVoxels.pop();
	}
	file_stream.close();
} /* outputPCDModel() */
//
//void MolecularSurface::outputSurfaceGrid(std::string const & filename, uint16_t margin) {
//	std::ofstream file_stream;
//	file_stream.open((filename + ".grid").c_str());
//	if (!file_stream.is_open()) {
//		throw std::ofstream::failure("Error opening output file.");
//	}
//	std::queue<voxel> surfaceVoxels;
//	for (int i = margin; i < plength-margin; ++i) {
//		for (int j = 0; j < pwidth; ++j) {
//			for (int k = 0 ; k < pheight; ++k) {
//				if (surface->getVoxel(i, j, k)) {
//					surfaceVoxels.push(voxel(i, j, k));
//				}
//			}
//		}
//	}
//	/* File header */
//	file_stream << "#.grid VoxSurf file format\n"
//			<< "LENGTH: " << plength << "\n"
//			<< "WIDTH: " << pwidth << "\n"
//			<< "HEIGHT: " << pheight << "\n"
//			<< "PROBE RADIUS: " << probeRadius << "\n"
//			<< "RESOLUTION: " << resolution << "\n"
//			<< "TRANSLATION VECTOR X Y Z: " << ptran.x << " " << ptran.y << " " << ptran.z << "\n"
//			<< "MARGIN: " << margin << "\n"
//			<< "NUMBER OF VOXELS: " << surfaceVoxels.size() << "\n"
//			<< "DATA\n";
//	while (!surfaceVoxels.empty()) {
//		file_stream << surfaceVoxels.front().ix << " "
//				<< surfaceVoxels.front().iy << " "
//				<< surfaceVoxels.front().iz << "\n";
//		surfaceVoxels.pop();
//	}
//	file_stream << "END";
//
//	file_stream.close();
//}



