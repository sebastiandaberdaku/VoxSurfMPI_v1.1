/*
 * MolecularSurface.h
 *
 *  Created on: Nov 11, 2013
 *      Author: daberdaku
 */
/**
 * This header defines the MolecularSurface object, representing
 * the surface of a molecule, with the related methods for it's
 * calculation.
 */
#ifndef MOLECULARSURFACE_H_
#define MOLECULARSURFACE_H_

#include "MolecularStructs.h"
#include "GeometryStructs.h"
#include "Pocket.h"
#include <queue>
#include <list>
#include <vector>

class MolecularSurface {
public:
	int rank;

	point3D ptran;
	std::list<atom> *atomList;
	float resolution;
	float probeRadius;
	uint16_t pheight, pwidth, plength;
	bool onlyHeavy; /**>True: heavy atoms only; false: all atoms (also Hydrogen).
	 	 	 	 	 * X-ray crystallography cannot resolve hydrogen atoms in
	 	 	 	 	 * most protein crystals, so in most PDB files, hydrogen
	 	 	 	 	 * atoms are absent. Sometimes hydrogens are added by
	 	 	 	 	 * modeling. Hydrogens are always present in PDB files
	 	 	 	 	 * resulting from NMR analysis, and usually present in
	 	 	 	 	 * theoretical models.*/
	bool addProbeRadius; /**> true: add the probe radius to the atomic radius
	 	 	 	 	 	  * values, SAS, SES and MS have this value set to true;
	 	 	 	 	 	  * false: use original atomic radius values. */
	float cutradis;		/**> cutoff value for the EDT calculation algorithm */
	float s_cutradis;

	voxelGrid *cpkModel, *surface, *temp;

	MolecularSurface(std::list<atom>* atomList, float probeRadius,
			float resolution, bool onlyHeavy, bool addProbeRadius, uint16_t length,
			uint16_t width, uint16_t height, point3D translation, int rank);
	static void boundingBox(std::list<atom> const & atomsInModel,
			float probeRadius, float resolution, bool onlyHeavy, bool addProbeRadius,
			uint16_t & length, uint16_t & width, uint16_t & height, point3D & translation);
	void createCPKModel();
	void spaceFillAtom(atom const & atm);
	void rollingSphere();
	void regionGrowingEDT();
	void fastRegionGrowingEDT();
	void buildSurface();
	void fillInternalCavities();
	void floodFill3D(voxel & startingVoxel);
	void outputBinvoxModel(std::string const & filename);
	void outputSurfacePCDModel(std::string const & filename, uint16_t margin);
//	void outputSurfaceGrid(std::string const & filename, uint16_t margin);
	void extractPocketVoxels(std::list<Pocket>* pocketList, uint16_t margin);
	virtual ~MolecularSurface();
};

#endif /* MOLECULARSURFACE_H_ */
